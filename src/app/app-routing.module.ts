import { ChangePasswordComponent } from './pages/my-account/change-password/change-password.component';
import { EmptyComponent } from './pages/checkout/empty/empty.component';
import { EditBeneficiaryFormComponent } from './pages/checkout/edit-beneficiary-form/edit-beneficiary-form.component';
import { PaymentComponent } from './pages/my-account/payment/payment.component';
import { BeneficiaryComponent } from './pages/my-account/beneficiary/beneficiary.component';
import { PersonalComponent } from './pages/my-account/personal/personal.component';
import { MyAccountComponent } from './pages/my-account/my-account.component';

import { CheckoutComponent } from './pages/checkout/checkout.component';
import { PolicyComponent } from './pages/policy/policy.component';

import { PoliciesComponent } from './pages/policies/policies.component';

import { ErroComponent } from './pages/erro/erro.component';
// import { LegalComponent } from './pages/terms/legal/legal.component';
// import { DisclamerComponent } from './pages/terms/disclamer/disclamer.component';
import { ConditionComponent } from './pages/terms/condition/condition.component';
import { ParametricComponent } from './pages/terms/parametric/parametric.component';
import { TermsComponent } from './pages/terms/terms.component';
import { FaqComponent } from './pages/faq/faq.component';
import { AboutComponent } from './pages/about/about.component';
import { HomeComponent } from './pages/home/home.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './services/auth.guard';





const routes: Routes = [
    { path: '', pathMatch: 'full', redirectTo: 'home' },
    { path: 'home', component: HomeComponent },
    { path: 'about', component: AboutComponent },
    { path: 'checkout', component: CheckoutComponent },
    { path: 'checkout/empty', component: EmptyComponent },
    { path: 'checkout/edit', component: EditBeneficiaryFormComponent },

    // { path: 'checkout/not-logged', component: NotLoggedComponent },
    { path: 'faq', component: FaqComponent },
    {
        path: 'my-account', component: MyAccountComponent, children: [
            {
                path: '', pathMatch: 'full', redirectTo: 'personal'
            },
            {
                path: 'personal', component: PersonalComponent
            },
            {
                path: 'beneficiary', component: BeneficiaryComponent
            },
            {
                path: 'payment', component: PaymentComponent
            },
            {
              path: 'change-password', component: ChangePasswordComponent
          },
        ]
    },
    { path: 'auth', loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule) },


    { path: 'policy/:id', canActivate: [AuthGuard]  ,component: PolicyComponent },
    {
        path: 'terms', component: TermsComponent, children: [
            {
                path: '', pathMatch: 'full', redirectTo: 'privacy'
            },
            {
                path: 'privacy', component: ParametricComponent
            },
            {
                path: 'condition', component: ConditionComponent
            }
        ]
    },
    {
        path: 'policies', pathMatch: 'full', redirectTo: 'policies/upcoming'
    },
    {
        path: 'policies/:option',  component: PoliciesComponent, canActivate: [AuthGuard]
    },
    { path: '**', component: ErroComponent },
    { path: 'error', component: ErroComponent },


];


@NgModule({
    imports: [RouterModule.forRoot(routes, { useHash: true })],
    exports: [RouterModule]
})
export class AppRoutingModule { }
