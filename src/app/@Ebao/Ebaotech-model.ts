import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { ModelRest } from './ModelRest';
import { EbaoAuth } from './EbaoAuth';

@Injectable({
    providedIn: 'root'
})
export  class EbaotechModel extends ModelRest {
    protected api = environment.ebao_tech;
    protected version = 'v1';
    protected path = '';
    public ready: boolean;
    constructor(
        protected http: HttpClient,
        protected auth: EbaoAuth
    ) { super(http); }


    public get url() {
        return this.api.url + this.api.tenantcode + this.api.main_path  + (this.version ? `${this.version}/` : '')
            + this.path + (this.custons.addUrl ? `/${this.custons.addUrl}` : '');
    }

    public get httpOptions() {

        const httpOptions = {
            headers: new HttpHeaders({
                Authorization: `Bearer ${this.auth.ebaoToken}`,
                'Content-Type': 'application/json',
                'x-ebao-tenant-code': environment.ebao_tech.tenantcode,
                ...this.api.headers
            })
        };
        return httpOptions;
    }




    protected handleServerError(response: HttpErrorResponse) {
        console.log(response, 'handleServerError');
        if (response.status === 403 || response.status === 401) {
           this.auth.authenticate(true);
        }

        if (response.status === 500) {
           
        }
    }
}
