import { isFunction } from 'util';
import * as moment from 'moment';

export abstract class EbaoModel {

    public patchValues(data: any) {
        // console.log('is passing here', data)
        for (let i in data) {
            if (this.hasOwnProperty(i)) {
                if (!isFunction(this[i])) { // check if is not a function
                    if (moment.isDate(data[i]) || moment.isMoment(data[i])) {
                        data[i] = moment(data[i]).locale(window.navigator.language)
                            .parseZone().format('Y-MM-D');
                    }
                    this[i] = data[i]; // put value;
                } else {
                    console.error(`Cannot patch value ${i}, cause it is not a valid one`);
                }
            }

        }
        return this;
    }

    public removeEmpty(removeEmptyArrays = false) {
        for (let i in this) {
            if (!this[i]) {
                delete this[i];
            }

            if (removeEmptyArrays) {
                if (Array.isArray(this[i])) {
                    let arr: any = this[i];
                    if (!arr.length) {
                        delete this[i];
                    }
                }
            }


        }

        return this;
    }
}