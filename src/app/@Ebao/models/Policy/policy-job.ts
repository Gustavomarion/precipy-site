import { PolicyRisk } from './policy-risk';
import { environment } from 'src/environments/environment';
import { EbaoModel } from '../EbaoModel';


export class JobItem extends EbaoModel {
    public ProductCode: string = environment.ebao_tech.product_schema.productCode;

    constructor(data: any, public PolicyRiskList:  PolicyRisk[] = []) {
        super();
        this.patchValues(data);
    }

}

// "PolicyLobList": [
    //         {
    //             "PolicyRiskList": [
    //                 {
    //                     "InsuredCategory": 1,
    //                     "InsuredSegment": 1,
    //                     "PolicyCoverageList": [
    //                         {
    //                             "ProductElementCode": "C101420",
    //                             "RainDepth": 0.4
    //                         }
    //                     ],
    //                     "ProductElementCode": "R10070"
    //                 }
    //             ],
    //             "ProductCode": "PRECP"
    //         }
    //     ]