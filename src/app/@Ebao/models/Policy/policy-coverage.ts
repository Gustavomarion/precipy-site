import { environment } from 'src/environments/environment';
import { EbaoModel } from '../EbaoModel';

export class PolicyCoverage  extends EbaoModel {
    public ProductElementCode: string = environment.ebao_tech.coverage.ProductElementCode;
    public RainDepth: number  | string  = '';
    public SumInsured: number | string  = '';
    public RainIntensity: number | string = '';
    constructor( data = {}) {
        super();
        this.patchValues(data);
    }
};
