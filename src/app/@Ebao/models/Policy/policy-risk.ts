import { PolicyCoverage } from './policy-coverage';
import { EbaoModel } from '../EbaoModel';
import { environment } from 'src/environments/environment';

export class PolicyRisk extends EbaoModel {
    public ProductElementCode: string = environment.ebao_tech.product_schema.ProductElementCode
    public InsuredCategory: string = '';
    public InsuredSegment: string = '';

    public Address?: string = '';
    public PostCode?: string= '';
    public AddressNumber?: string= '';
    public AddressComplement?: string = '';
    public Suburb?: string= '';
    public City?: number | string = '';
    public State?: number | string = '';
    public Latitude: number | string = '';
    public Longitude: number | string = '';

    public ConstructionPeriodStart: string = '';
    public ConstructionPeriodEnd: string = '';

    public VROCC: number | string = '';

    public FormattedAddress: string = '';

    constructor(data: any, public PolicyCoverageList: PolicyCoverage[] = []) {
        super();
        this.patchValues(data);
    }
    
    
    
}
