import { EbaoModel } from '../EbaoModel';
import { QuestionBoolean } from '../Customer/customer-identification';


export class PolicyPayment extends EbaoModel {
    public IsInstallment?: QuestionBoolean  = '';
    public InstallmentType?: string | number = '';
    public PayModeCode?: string | number = '';
    public ExpiryDate: string = '';
    public InstallmentPeriodCount: string | number = '';

    constructor(data) {
        super();
        this.patchValues(data);
    }
}