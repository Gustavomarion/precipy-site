import { JobItem } from './policy-job';
import { Customer } from '../Customer/customer';
import { PolicyRisk } from './policy-risk';
import { EbaoModel } from '../EbaoModel';
import { environment } from 'src/environments/environment';
import { PolicyCoverage } from './policy-coverage';
import { PolicyPayment } from './policy-payment';
import { concatOrPush } from '../../functions/helpers';
import { QuestionBoolean } from '../Customer/customer-identification';

export class Policy extends EbaoModel {
    public ProductCode?: string = environment.ebao_tech.product_schema.productCode;
    public ProductVersion?: string = environment.ebao_tech.product_schema.version;
    public ProposalNo?: string = '';
    public ProposalStatus: string = '';
    public ProposalDate?: string = '';
    
    public EffectiveDate?:any = '';
    public EffectiveTime?: string = '';
    public ExpiryDate?: any= '';
    public ExpiryTime?: string = '';

    public PolicyStatus: string = '';
    public PolicyCustomerList?: Customer[] = [];
    public PolicyLobList?: JobItem[] = [];
    public PolicyPaymentInfoList: PolicyPayment[] = [];
    public TotalPremium: any  = '';
    public TaxInterest: any  = '';
    public DuePremium: any  = '';
    // public EffectiveDate: string;
    public BeneficiaryName: any  = '';
	public BeneficiaryEmail: any  = '';
	public BeneficiaryMessage: any  = '';
	public BeneficiaryPersist: QuestionBoolean = '';
	public BeneficiaryId: any  = '';
	
	public BeneficiaryPayoutId: any  = '';
	public BeneficiaryPayoutLast4: any  = '';
	public BeneficiaryPayoutToken: any  = '';
    public BeneficiaryPayoutPersist: QuestionBoolean = '';



    public  BeforeVatPremium:any  = '';
    public  BusinessCateCode: any  = '';
    public  CommisionRate: any  = '';
    public  Commission: any  = '';
    public  ProductId: any  = '';

    public PolicyNo: any  = '';

    public AgentCode: any  = '' // pegar da url;
    public OrgCode: any  = '';
    constructor(data: any = {}) {
        super();
        this.PolicyLobList.push(new JobItem(data));
        this.AgentCode = environment.ebao_tech.tenantcode;
        this.patchValues(data);
    }

    public coverages(coverage: PolicyCoverage | PolicyCoverage[]) {
        let coverageList = this.PolicyLobList[0].PolicyRiskList[0].PolicyCoverageList;
        this.PolicyLobList[0].PolicyRiskList[0].PolicyCoverageList = concatOrPush(coverageList, coverage, PolicyCoverage);
        return this;
    }

    public risk(risk: PolicyRisk | PolicyRisk[]) {
        let risksList = this.PolicyLobList[0].PolicyRiskList;
        risksList = risksList ? risksList : [];
        this.PolicyLobList[0].PolicyRiskList = concatOrPush(this.PolicyLobList[0].PolicyRiskList, risk, PolicyRisk);

        if (!this.PolicyLobList[0].PolicyRiskList.length) {
            console.warn('No risk to save');
        }
        return this;
    }

    public customer(customer: Customer | Customer[]) {
        this.PolicyCustomerList = concatOrPush(this.PolicyCustomerList, customer, Customer);
        if (!this.PolicyCustomerList.length) {
            console.warn('no customer to save');
        }
        return this;
    }

  
    public policyHolder(customer: Customer) {
        let hasnt = true;
        const policyHolder = new Customer(customer);
        policyHolder.policyHolder();
        this.PolicyCustomerList.map(e => {
            if(e.IsPolicyHolder == 'Y') {
                e = policyHolder ;
                hasnt  = false;
            }
        });

        if(hasnt) {
            this.PolicyCustomerList.push(policyHolder);
        }

    }


    public payment(payment: PolicyPayment) {
        this.PolicyPaymentInfoList = concatOrPush(this.PolicyPaymentInfoList, payment, PolicyPayment);
        if (!this.PolicyPaymentInfoList.length) {
            console.warn('No payment info to save');
        }

        return this;
    }


    public makePolicyHolder(customer: Customer) {
        let idx = this.PolicyCustomerList.indexOf(customer);
        if(idx > -1) {
            this.PolicyCustomerList.map((e) => e.IsPolicyHolder = 'N');
            this.PolicyCustomerList[idx].IsPolicyHolder = 'Y';
        } else if( Object.keys(customer).length) {
            let policyHolder = new Customer(customer);
            policyHolder.policyHolder();
            this.PolicyCustomerList.push(policyHolder);
        } else {
            console.error('No policy holder finded or created. Verify your data informations');
        }
        return this;
    }

    public prepareRequest() {
        this.removeEmpty();
        for (let customer of this.PolicyCustomerList) {
            customer.removeEmpty();
        }
        for (let jobItem of this.PolicyLobList) {
            jobItem.removeEmpty();
            for (let jobRisk of jobItem.PolicyRiskList) {
                jobRisk.removeEmpty();
                for (let coverage of jobRisk.PolicyCoverageList) {
                    coverage.removeEmpty();
                }
            }
        }
        return this;
    }



    public static clone(data: any) {
        const policyClone = new Policy(data);
        delete policyClone.ProposalNo;
        return policyClone;
    }

}




//             ProductCode: this.productService.product.schema.BusinessCode,
//             ProductVersion: this.productService.product.schema.ProductVersion,
//             ProposalDate: this.helper.date(today).format("Y-MM-DD"),
//             EffectiveDate: this.helper
//                 .date(today)
//                 .add(5, "days")
//                 .format("Y-MM-DD"),
//             ExpiryDate: this.helper
//                 .date(today)
//                 .add(20, "days")
//                 .format("Y-MM-DD"),
//             AgentCode: "PTY370059801",
//             BookCurrencyCode: "BRL",
//             BusinessCateCode: "1",
//             PolicyType: "1",
//             OrgCode: "ROOT",
//             CommisionRate: 20,
//             QuotationStatus: 1,
//             DocName: 'Certificado',
//             DocTitle: 'Dados do Certificado',
//             ClaimsNotification: '(11)5555-4444',
//             PolicyCustomerList: this.customerService.formatedCustomers(
//                 this.responsavel.value
//             ),
//             PolicyPaymentInfoList: [
//                 {
//                     PayModeCode: "70",
//                     IsInstallment: "Y",
//                     InstallmentType: "1",
//                     FirstInstallmentDate: this.helper
//                         .date(today)
//                         .add(13, "days")
//                         .format("Y-MM-DD")
//                 }
//             ],
//             PolicyAgentList: [
//                 {
//                     AgentCode: "PTY370059801",
//                     ShareRate: 0.8
//                 },
//                 {
//                     AgentCode: "PTY370063009",
//                     ShareRate: 0.2
//                 }
//             ],
//             PolicyLobList: [
//                 {
//                     ProductCode: this.productService.product.schema.BusinessCode,
//                     PolicyRiskList: [
//                         {
//                             ProductElementCode: this.productService.product
//                                 .ProductElementProductRisk[0].ProductElementCode,
//                             ...this.form.value.descricaoObra,
//                             ...this.formatedAddress(),
//                             ...this.form.value.obra,
//                             ...{
//                                 ConstructionPeriodStart: this.helper
//                                     .date(this.periodoobraForm.value.ConstructionPeriodStart)
//                                     .format("Y-MM-DD"),
//                                 ConstructionPeriodEnd: this.helper
//                                     .date(this.periodoobraForm.value.ConstructionPeriodEnd)
//                                     .format("Y-MM-DD"),
//                                 Comm_DiscLoad: 0
//                             },
//                             PolicyCoverageList: this.coverages
//                         }
//                     ]
//                 }
//             ]