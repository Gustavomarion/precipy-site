import { Policy } from './policy';
import { environment } from 'src/environments/environment';

export class PolicyRequest {
    Conditions?: Policy = {} as Policy;
    ToRangeConditions?: any = {};
    FromRangeConditions?: any = {};
    PageNo?: number = 1;
    PageSize?: number =  1000;
    SortField?: string =  'index_time';
    SortType?: string = 'desc';
    Module?: string = 'Policy';

    FuzzyConditions: any = {};
    OrConditions:any =  {};
    InConditions:any =  {};
    NotInConditions:any  = {}
    constructor() {
        this.Conditions.ProductCode = environment.ebao_tech.product_schema.productCode;
    }
}