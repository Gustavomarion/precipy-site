export type CustomerTypes = 'IndiCustomer' | 'IndiOrg' | '';
export type CustomerIdTypes = '1' | '2' | 1 | 2 ;
export type QuestionBoolean = 'Y' | 'N' | '';

export class CustomerIdentification {
    public Type: CustomerTypes  =  'IndiCustomer';
    public IdType: CustomerIdTypes = '1';
    public CustomerType: CustomerIdTypes

    constructor(
        public IdNo?: string
    ) {
        this.setId(this.IdNo);
    }

    public setId(id: string) {
        if(id.length == 11) {
            this.Type = 'IndiCustomer';
            this.IdType = 1;
            this.CustomerType = 1;
        } else {
            this.Type = 'IndiOrg';
            this.IdType = 2;
            this.CustomerType = 2;
        }
    }
}