import { isFunction } from 'util';
import { CustomerTypes, CustomerIdTypes, QuestionBoolean } from './customer-identification';
import { EbaoModel } from '../EbaoModel';
import { environment } from 'src/environments/environment';

export class Customer extends EbaoModel {
    public Type: CustomerTypes = '';
    public IdType: CustomerIdTypes = '1';
    public CustomerType: '1';

    public CustomerName: string = '';
    public Gender: string = '';
    public DateOfBirth: string = '';
    public Occupation: string = '';
    public Email: string = '';
    public Mobile: string = '';

    public IsPep: QuestionBoolean = '';
    public IsPolicyHolder: QuestionBoolean = '';
    public IsBeneficiary: QuestionBoolean = '';

    public ContactInfoList?: any[];

    public ConsentData: QuestionBoolean = '';
    public ConsentEmail: QuestionBoolean = '';
    public ConsentWhatsApp: QuestionBoolean = '';
    public EstimateAsset?: number | string = '';
    public MonthlyIncome?: number | string = '';
    public IdNo: string = '';

    public City: number | string = '';
    public State: number | string = '';
    public PostCode: string | number = '';
    public Address: string = '';
    public AddressNumber: string | number = '';
    public AddressComplement: string = '';
    public Suburb: string = '';
    constructor(
        data: any
    ) {
        super();
        this.patchValues(data);
        if (environment.ebao_tech.customer.default_IdNo) {
            this.setDefaultId();
        }

    }

    public setDefaultId() {
        let defaultIndice = 'Email';
        defaultIndice = environment.ebao_tech.customer.default_IdNo;
        this.IdNo = this[defaultIndice];
    }


    public policyHolder() {
        this.IsPolicyHolder = 'Y';
        this.IsBeneficiary = 'N';
        return this;
    }
}


// "CustomerName": "Customer",
// "DateOfBirth": "1988-10-01",
// "IdType": "1",
// "IdNo": "teste123@gmail.com",
// "City": "3550308",
// "State": "35",
// "Mobile": "(11)94800-5943",
// "Email": "teste123@gmail.com",
// "PostCode": "04113",
// "Address": "Rua Pero Correia, 916 Casa 2",
// "AddressNumber": "Test1",
// "AddressComplement": "Test2",
// "Suburb": "Test3",
// "CustomerType": "1",
// "IsPolicyHolder": "Y"