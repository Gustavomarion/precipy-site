import { Observable, throwError, timer } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { Injectable } from '@angular/core';



export type optionsRequest = {bffUrl?: boolean, forceCors?: boolean, dataInBody?: boolean};
@Injectable({
    providedIn: 'root'
})
export  class ModelRest {
    protected api: any;
    protected version = '';
    protected path = '';
    public element: any;
    public elements: any[];

    public custons: any = {
        addUrl: ''
    };

    public herokuCors = 'https://cors-anywhere.herokuapp.com/';
    public errorToastr: any;
    constructor(
        protected http: HttpClient
    ) {
        this.custons = {};
    }
    public get url() {
        return this.api.url + (this.version ? `${this.version}/` : '')
            + this.path + (this.custons.addUrl ? `/${this.custons.addUrl}` : '');
    }

    public get hostPath() {
        return `${this.api.url}${this.api.tenantcode}/noble.1.0/${this.path}` ;
    }

    public get host() {
        return `${this.api.url}${this.api.tenantcode}/noble.1.0` ;
    }


    public get httpOptions() {
        const httpOptions = {
            headers: new HttpHeaders()
        };

        return httpOptions;
    }

    public request(method: 'get' | 'post' | 'put' | 'delete' | 'patch', path = '', data?: any, options: optionsRequest = {}): Observable<any> {
        let request = new Observable<any>();
        if (!method) {
            console.error('declarar o metodo do request');
            return null;
        }

        this.beforeRequest();
        // console.log(this.httpOptions().headers)
        let url = this.url + path;

        if (path.includes('http')) {
            url = path;
        }

       

        if(options.bffUrl == false) {
            url = this.hostPath + path;
        }

        if(options.forceCors  == true) {
            url = this.herokuCors+url;
        }

        if (this.custons.force_method) {
            method = this.custons.force_method;
        }

        if (this.custons.noLoading) {
            data.noLoading = true;
        }
        if (method === 'get' || method === 'delete' ) {
            
            request = this.http[method](url + this.parans(data), this.httpOptions);
        } else {
            request =  this.http[method](url, data, this.httpOptions) ;
        }

        return request.pipe(
            catchError(error => this.handleError(error))
        );
    }

    public get(parans?: any): Observable<any> {
        return this.request('get', '', parans);
    }

    public find(id: number | any) {
        return this.request('get', `/${id}`);
    }

    public create(data: any): Observable<any> {
        return this.request('post', '', data);
    }

    public update(id: number, data: any): Observable<any> {
        return this.request('put', `/${id}`, data);
    }

    public delete(id: number): Observable<any> {
        return this.request('delete', `/${id}`);
    }

    public cleanOptions() {
        this.custons = {};
        return this;
    }
    public options(options) {
        this.custons = options;
        return this;
    }


    public beforeRequest() {

    }


    public requestPost() {
        return this.request('post');
    }

    public requestGet() {
        return this.request('post');
    }
    

    public parans(parans) {
        let urlParans = '?';
        for (const i in parans) {
            if (i) {
                urlParans += `${i}=${JSON.stringify(parans[i])}&`;
            }
        }
        urlParans = urlParans.replace(/[%]/g, '%25');
        urlParans = urlParans.replace(/["]/g, '');
        return urlParans;
    }

    protected handleServerError(response: HttpErrorResponse) {
        console.log(response);
    }

    public handleError(response) {
        let errorMessage = '';
        if (response.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            errorMessage = response.error.message;
        } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            this.handleServerError(response);
            errorMessage = 'Internal server error';
        }
        return throwError(response.error);
    }
}
