import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LocalStorageService } from 'angular-web-storage';
import { environment } from 'src/environments/environment';
import { ModelRest } from './ModelRest';
import { interval } from 'rxjs';
@Injectable({
    providedIn: 'root'
})
export  class EbaoAuth extends ModelRest {
    public user: any;
    public ebaoToken: string;
    public api = environment.ebao_tech;
    constructor(
        protected http: HttpClient,
        private session: LocalStorageService
    ) {
        super(http);
        this.ebaoToken = this.session.get('ebao-token');
    }

    public authenticate(force = false, msg = true) {
        if (!this.session.get('ebao-token') || force) {
            this.request('post', `${this.api.cas}ebao/v1/json/tickets`, this.api.credentials).subscribe((response: any) => {
                this.session.set('ebao-token', response.access_token);
                this.ebaoToken = response.access_token;
                if(msg ) {
                    this.showStatusAuthenticated('New');
                }

               
            });
        } else {
            this.showStatusAuthenticated();
        }
    }

    public init() {
        this.authenticate();
        const authenticateTime = interval(500000);
        authenticateTime.subscribe(() => {
            this.authenticate(true, false);
        });
        
    }

    protected showStatusAuthenticated(moreText = '') {
        console.log('%c Ebao is on...' ,
            'color: green; font-size:10px;  background-color: #303030;', moreText);
    }
}