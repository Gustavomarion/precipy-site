import { isFunction } from 'util';
import * as moment from 'moment';

export function patchValues(data: any, obj: any) {
    for (let i in data) {
        if (obj[i]) { // verify if exists this data
            if (!isFunction(obj[i])) { // check if is not a function
                if (moment.isDate(data[i]) || moment.isMoment(data[i])) {
                    data[i] = moment(data[i]).locale(window.navigator.language)
                        .parseZone().format('Y-MM-D');
                }
                obj[i] = data[i]; // put value;
            } else {
                console.error(`Cannot patch value ${i}, cause it is not a valid one`);
            }
        }
    }
    return obj;
}

export function concatOrPush(obj: any[] = [], value: any | any[], entity: any) {
    if (Array.isArray(value)) {
        obj = obj.concat(value.map(i => new entity(value)));
    } else if (Object.keys(value).length) {
        obj.push(new entity(value));
    }

    return obj;
}