import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { EbaotechModel } from '../Ebaotech-model';
import { EbaoAuth } from '../EbaoAuth';
import { CustomerIdentification } from '../models/Customer/customer-identification';

@Injectable({
    providedIn: "root"
})
export class CustomerService extends EbaotechModel {
    public path = "customer";
    constructor(
        protected http: HttpClient,
        protected auth: EbaoAuth
    ) {
        super(http, auth);
    }
    public check(idNo: string) {
        return this.request("post", "/check", new CustomerIdentification(idNo));
    }
    public create(data: any): Observable<any> {
        return this.request("post", "/create", data);
    }

    public update(id: number, data: any): Observable<any> {
        return this.request("post", `/update`, data);
    }


}
