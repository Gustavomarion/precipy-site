import { EbaotechModel } from '../Ebaotech-model';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { EbaoAuth } from '../EbaoAuth';
import { map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class ProductService extends EbaotechModel {
    public product: any;
    protected path = 'product';
    public infoData: any[] = [];
    constructor(protected http: HttpClient, public auth: EbaoAuth) {
        super(http, auth);
    }

    public init() {
        return this.find().subscribe((product: any) => {
            if(!this.product) {
                this.findAttributes(product)
            }
        });
    }

    public getInfo(code) {
        let info = this.infoData.find(e => e.Code == code);
        return info? info: {};
    }

    public find(params: any = environment.ebao_tech.product_schema) {
        return this.request('get', '/schema', params);
    }
    public findAttributes(productSchema: any) {
        if (productSchema.ChildProductTreeNodeList) {
            // productSchema.ChildProductTreeNodeList.forEach((e: any, i: number) => {
            //     let type: string = e['@type'];
            //     type = type.replace(/[-]/g, '');
            //     if (!this.product[type]) {
            //         this.product[type] = [];
            //     }
            //     this.product[type].push(e);
            //     this.findAttributes(e);
            // });
        }
    }

    public info(codeTable: string) {
        return this.request('get', '/codetable', { codeTableName: codeTable }).pipe(
            map(response => response.BusinessCodeTableValueList)
        );
    }

    public rules(codeTable: string, params: any = environment.ebao_tech.product_schema) {
        return this.request('post', `/lookup?code=${codeTable}&version=1`, params);
    }
}
