import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { PolicyRequest } from '../models/Policy/policy-request';
import { EbaotechModel } from '../Ebaotech-model';
import { EbaoAuth } from '../EbaoAuth';
import { Policy } from '../models/Policy/policy';

@Injectable({
    providedIn: 'root'
})
export class PolicyService extends EbaotechModel {
    protected path = 'policy';
    public status: any = {
        1: { label: 'Pendente', style: 'btn-cotacao-pendente' },
        2: { label: 'Liberada', style: 'btn-cotacao-liberada' },
        3: { label: 'Rejeitada', style: 'btn-cotacao-rejeitada' }, // cotação rejeitada
        4: { label: 'Excluida', style: 'btn-cotacao-excluida' },
        5: { label: 'Não autorizada', style: 'btn-cotacao-unauthorized' }, // aparece em propostas
        6: { label: 'Aprovada', style: 'btn-cotacao-aprovada' }
    };

    constructor(
        protected http: HttpClient,
        protected auth: EbaoAuth
    ) {
        super(http, auth);
    }

    public getAll(params: PolicyRequest = new PolicyRequest(), claim = false) {
        const payload: PolicyRequest = new PolicyRequest();
        for (let i in params) {
            if (payload[i]) {
                payload[i] = params[i];
            }
        }
        // console.log(payload);
        let path = 'query';
        if(claim) {
            path += 'Claimed';
        }
        return this.options({ addUrl:  path, force_method: 'post' })
            .get(payload).pipe(
                map((response: any) => {
                    const newResponse: any = {
                        GroupField: 'entity_type',
                        PageNo: response.PageNo,
                        PageSize: response.PageSize,
                        Total: response.Total as Policy[],
                        list: []
                    };
                    if (response.Total) {
                        response.Results.forEach(element => {
                            newResponse.list = newResponse.list.concat(
                                element.EsDocs
                            );
                        });
                    }
                    return newResponse;
                })
            );
    }
    public find(id: any): Observable<any> {
        return this.request('get', '/load', { proposalNo: id });
    }
    public update(data: any): Observable<any> {
        return this.request('post', '/save', data);
    }

    public fullQuotation(data: any): Observable<any> {
        return this.request('post', '/fullquote', data);
    }
    public quickQuotation(data: any): Observable<any> {
        return this.request('post', '/quickquote', data);
    }

    public getCoverages(apolice: any) {
        const coverages: any[] = [];
        apolice.PolicyLobList.forEach((policy: any) => {
            policy.PolicyRiskList.forEach((risk: any) => {
                risk.PolicyCoverageList.forEach((coverage: any) => {
                    coverages.push(coverage);
                });
            });
        });
        return coverages;
    }


    public bind(data: any) {
        return this.request('post', '/bind', data);
    }

    public buy(data: any): Observable<any> {
        return this.request("post", this.url.replace('1.0/iam01-bff-app/v1/policy', 'noble.1.0/buy'), data);
    }


    public cancel(id) {
        return this.request('post', `/cancel`, {PolicyNo: id}, {bffUrl: false});
    }


    public updateBeneficiary(data: any) {
        return this.request('post', '/updateBeneficiary', data, { bffUrl: false });
    }

}


