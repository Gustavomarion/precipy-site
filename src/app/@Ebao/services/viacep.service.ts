import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { ModelRest } from '../ModelRest';

@Injectable({
    providedIn: 'root'
})
export class ViaCepService extends ModelRest {
    protected api = {url: 'https://viacep.com.br/ws/'};
    constructor(protected http: HttpClient) { super(http); }

    public find(cep: string) {
        return this.http.get(`${this.url}${cep}/json`).pipe(
            map((response: any) => {
                cep = cep.replace(/[-]/, '');
                if (!response.erro && cep.length === 8) {
                    return {
                        Address:  `${response.logradouro} - ${response.localidade}, ${response.uf}`,
                        AddressComplement: response.complemento,
                        Suburb: response.bairro,
                        Code: response.cep,
                        City: response.ibge,
                        State: response.ibge.substr(0, 2),
                    };
                } else {
                    return throwError({error: 'Cep invalido'});
                }
            })
        );
    }
}
