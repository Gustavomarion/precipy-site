import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { EbaoAuth } from '../EbaoAuth';
import { EbaotechModel } from '../Ebaotech-model';
import { PaymentMethod } from 'ngx-stripe/lib/interfaces/payment-intent';
import { tap } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Policy } from '../models/Policy/policy';

@Injectable({
    providedIn: 'root'
})
export class PaymentService extends EbaotechModel {
    protected path = 'payment';
    public beneficiaries: any[] = [];
    public paymentMethods: PaymentMethod[] | any[]=  [];
    public payIdx: number;
    constructor(protected http: HttpClient, public auth: EbaoAuth) {
        super(http, auth);
    }

    get selectedMethod() {
        return this.paymentMethods[this.payIdx]? this.paymentMethods[this.payIdx]: {} as PaymentMethod;
    }

    get isAblePayment() {
        return this.payIdx != null || this.payIdx != undefined? true: false;
    }
    public payModes(data) {
        return this.request('get', '/payMode', data, { bffUrl: false });
    }

    public create(data: any) {
        return this.request('post', '/payMode', data, { bffUrl: false });
    }

    public delete(id) {
        return this.request('delete', '/payMode', {
            PaymentMethodId: id
        }, { bffUrl: false, forceCors: true });
    }

    public update(data: any) {
        return this.request('put', '/payMode', data, { bffUrl: false, forceCors: true });
    }



  
    public getPayouts(id) {
        return this.request('get', '/beneficiary', {BeneficiaryId: id}, { bffUrl: false });
    }

    public getBeneficiaries(email, force = false) {
        if(!this.beneficiaries.length || force) {
            return this.request('get', '/beneficiary', {CustomerMail: email}, { bffUrl: false }).pipe(
                tap(res => this.beneficiaries = res)
            );
        } 
    }

    public deleteBeneficary(data) {
        return this.request('delete', '/beneficiary', data, { bffUrl: false, forceCors: true });
    }

    public editBeneficary(data) {
        return this.request('put', '/beneficiary', data, { bffUrl: false, forceCors: true });
    }

    public createBeneficary(data) {
        return this.request('post', '/beneficiary', data, { bffUrl: false, forceCors: true });
    }

    public make(policies: Policy[], beneficiaryInfo?) {
        return this.request('post', this.host+ '/buyList', {
            PolicyObjectList: policies.map(e => {
                e = new Policy(e);
                if(!e.BeneficiaryName && !e.BeneficiaryId && beneficiaryInfo) {
                    e.patchValues(beneficiaryInfo) 
                }
                return e.removeEmpty();
            }),
            PaymentInfo: {
                paymentMethodId: this.selectedMethod.id,
                customerId: this.selectedMethod.customer
            }
        });
    }

    public selectPaymentMethod(payment: number | PaymentMethod) {
        if(typeof payment == 'number') {
            this.payIdx = payment;
        } else {
            let idx = this.paymentMethods.indexOf(payment);
            this.payIdx = idx > -1? idx: null;
        }
    }
    public addPaymode(payMode: PaymentMethod) {
        this.paymentMethods.push(payMode);
    }


    public editPaymode(idx, payMode: PaymentMethod){
        this.paymentMethods[idx] = payMode;
    }
}
