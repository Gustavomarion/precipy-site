import { Component, OnInit } from '@angular/core';
import { CartService } from 'src/app/services/cart.service';
import { PolicyService } from 'src/app/@Ebao/services/policy.service';
import { Policy } from 'src/app/@Ebao/models/Policy/policy';
import { HelperService } from 'src/app/services/helper.service';
import { MatDialog } from '@angular/material';
import { EditBeneficiaryFormComponent } from './edit-beneficiary-form/edit-beneficiary-form.component';
import { AuthService } from 'src/app/services/auth.service';
import { ModalPaymentOutComponent } from './modal-payment-out/modal-payment-out.component';
import { PaymentService } from 'src/app/@Ebao/services/payment.service';
import { CustomerService } from 'src/app/@Ebao/services/customer.service';
import { Router } from '@angular/router';
import { ProductService } from 'src/app/@Ebao/services/product.service';
import { timeList } from 'src/app/.shared/times';

@Component({
    selector: 'app-checkout',
    templateUrl: './checkout.component.html',
    styleUrls: ['./checkout.component.scss'],
    providers: [PolicyService, CustomerService]
})
export class CheckoutComponent implements OnInit {


    public loading: boolean;
    public infoData: any;
    public timeList: any[] = timeList;
    public error: string;
    deletingIdx
    duplicatingIdx
    constructor(
        public cart: CartService,
        public helper: HelperService,
        public dialog: MatDialog,
        public auth: AuthService,
        public  pay: PaymentService,
        private router: Router,
        public productService: ProductService
        ) { }

    ngOnInit() {
        this.redirectIfEmpty();
        console.log(this.cart.items)
    }


    public redirectIfEmpty() {
        if(!this.cart.items.length) {
            this.router.navigate(['/checkout/empty']);
        }
    }
    public duplicate(idx) {
        this.cart.editIdx = idx;
        this.helper.openQuote(false, 'duplicate');
    }

    public edit(policy: Policy, idx) {
        this.cart.editIdx = idx;
        this.helper.openQuote(false, 'edit-quote');
    }

    public editBeneficiary(policy, idx, justNote = false) {
        if(this.auth.isLoggedIn) {
            const dialogRef = this.dialog.open(EditBeneficiaryFormComponent, {
                width: 'auto',
                maxWidth: '781px',
                height: 'auto',
                data: {idx: idx, policy: policy, justNote: justNote},
                id: 'edit-beneficiary'
            });
        }
        
    }

    public paymentOut(policy, idx) {
        if(this.auth.isLoggedIn) {
            const dialogRef = this.dialog.open(ModalPaymentOutComponent, {
                width: 'auto',
                maxWidth: '781px',
                height: 'auto',
                data: {idx: idx, policy: policy},
                id: 'payment-out-modal'
            });
        }
       
    }

    public delete(idx) {
        this.cart.remove(idx);
        this.redirectIfEmpty();
    }


    public doPayment() {
        // this.pay.selectedMethod
        this.loading = true;
        this.pay.make( this.cart.items, {
            BeneficiaryName: this.auth.user.name,
            BeneficiaryEmail: this.auth.user.email
        }).subscribe((response) => {
            if(!response.status) {
                this.router.navigate(['/policies', 'upcoming']);
                this.cart.removeAll();
            } else {
                this.error = '500';
                this.router.navigate(['/error', {payment: 1, checkout: 1}])
            }

        }, error => {
            this.error = '500';
            this.router.navigate(['/error', {payment: 1, checkout: 1}])
        }).add(() =>  this.loading = false);
    }
}
