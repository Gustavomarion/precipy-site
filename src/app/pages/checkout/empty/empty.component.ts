import { Component, OnInit } from '@angular/core';
import { HelperService } from '../../../services/helper.service';


@Component({
  selector: 'app-empty',
  templateUrl: './empty.component.html',
  styleUrls: ['./empty.component.scss']
})
export class EmptyComponent implements OnInit {

  constructor(public helper: HelperService) { }

  ngOnInit() {
  }

}
