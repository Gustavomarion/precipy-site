import { Component, OnInit, ViewChild } from '@angular/core';
import { StripeService, Elements, Element as StripeElement, ElementsOptions, StripeCardComponent, ElementOptions } from 'ngx-stripe';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SatDatepicker, MAT_DATE_FORMATS } from 'saturn-datepicker';
import { Moment } from 'moment';
import { HelperService } from 'src/app/services/helper.service';
import { PaymentService } from 'src/app/@Ebao/services/payment.service';
import { AuthService } from 'src/app/services/auth.service';
import { PaymentMethod } from 'ngx-stripe/lib/interfaces/payment-intent';
import { MatDialog } from '@angular/material';
import { EditPaymentComponent } from './edit-payment/edit-payment.component';
import { retry } from 'rxjs/operators';

declare const Stripe: any;
@Component({
    selector: 'app-pay',
    templateUrl: './pay.component.html',
    styleUrls: ['./pay.component.scss'],
    providers: [
    ]
})
export class PayComponent implements OnInit {
    @ViewChild(StripeCardComponent, { static: false }) card: StripeCardComponent;
    public showHideNewMethod;
    public customPatterns = { '0': { pattern: new RegExp('-|[0-9]') }, '9': { pattern: new RegExp('[0-9]') } };
    public form: FormGroup = this.fb.group({
        card: this.fb.group({
            name: ['', Validators.required]
        }),
        account: this.fb.group({
            account_holder_name: ['', Validators.required],
            routing_number: ['', Validators.required],
            account_number: ['', Validators.required],
            // dados fixos
            account_holder_type: ['individual'],
            country: ['US'],
            currency: ['usd']
        }),
        payment_method: ['card']
    });

    cardOptions: ElementOptions = {
        style: {
            base: {
                iconColor: '#666EE8',
                color: '#31325F',
                lineHeight: '40px',
                fontWeight: 300,
                fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                fontSize: '18px',
                '::placeholder': {
                    color: '#CFD7E0'
                }
            }
        },
        value: {
        },
        hidePostalCode: false
    };

    elementsOptions: ElementsOptions = {
        locale: 'en',
    };

    public loadings: any = {};
    public deletingIdx: number;
    constructor(
        private stripe: StripeService,
        private fb: FormBuilder,
        public helper: HelperService,
        public pay: PaymentService,
        private auth: AuthService,
        public dialog: MatDialog,
    ) { }

    get cardForm() {
        return this.form.get('card') as FormGroup;
    }

    get bankAccountForm() {
        return this.form.get('account') as FormGroup;
    }
    ngOnInit() {
        this.payModeList();
    }

    payModeList() {
        this.loadings.paymodes = true;
        this.pay.payModes({ CustomerMail: this.auth.user.email }).pipe(
            retry(2)
        ).subscribe((response: PaymentMethod[]) => {
            this.pay.paymentMethods = response;
            if (response.length == 1) {
                this.pay.selectPaymentMethod(response[0]);
            }
        }).add(() => this.loadings.paymodes = false);
    }


    protected saveMethod(response) {
        if (response.error) {
            this.form.setErrors({ stripe: response.error.message })
            this.loadings.addPaymentMethod = false;
        } else {
            this.pay.create({
                PaymentToken: response.token.id,
                CustomerMail: this.auth.user.email
            }).subscribe((response) => {
                this.pay.addPaymode(response);
                this.pay.selectPaymentMethod(response);
                this.showHideNewMethod = false;
                this.cardForm.reset();
            }).add(() => this.loadings.addPaymentMethod = false);
        }
    }

    public addCardMethod() {
        let name = this.auth.user.name;
        this.stripe.createToken(this.card.getCard(), { name }).subscribe((response) => this.saveMethod(response));
    }


    public removeMethod(id, idx) {
        // console.log(id);
        this.deletingIdx = idx;
        this.pay.delete(id).subscribe((response) => {
            this.pay.paymentMethods.splice(idx, 1);
        }).add(() => this.deletingIdx = null);
    }

    public addBankMethod() {
        this.stripe.createToken('bank_account', this.bankAccountForm.value).subscribe((response)  => this.saveMethod(response));
    }


    public methodEdit(method, idx) {
        const dialogRef = this.dialog.open(EditPaymentComponent, {
            width: 'auto',
            maxWidth: '781px',
            height: 'auto',
            data: {idx: idx, method: method},
            id: 'payment-modal'
        });
    }
    public submit() {
        this.loadings.addPaymentMethod = true;
        if (this.form.get('payment_method').value == 'card') {
            this.addCardMethod();
        } else if (this.form.get('payment_method').value == 'bank') {
            this.addBankMethod();
        }
    }




}
