import { Component, OnInit, Optional, Inject, ViewChild } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ModalPaymentOutComponent } from '../../modal-payment-out/modal-payment-out.component';
import { CustomerService } from 'src/app/@Ebao/services/customer.service';
import { PaymentService } from 'src/app/@Ebao/services/payment.service';
import { CartService } from 'src/app/services/cart.service';
import { StripeService, StripeCardComponent, ElementOptions, ElementsOptions } from 'ngx-stripe';
import { AuthService } from 'src/app/services/auth.service';

@Component({
    selector: 'app-edit-payment',
    templateUrl: './edit-payment.component.html',
    styleUrls: ['./edit-payment.component.scss']
})
export class EditPaymentComponent implements OnInit {
    @ViewChild(StripeCardComponent, { static: false }) card: StripeCardComponent;
    public form: FormGroup = this.fb.group({
        account: this.fb.group({
            account_holder_name: ['', Validators.required],
            routing_number: ['', Validators.required],
            account_number: ['', Validators.required],

            // dados fixos
            account_holder_type: ['individual'],
            country: ['US'],
            currency: ['usd']
        }),
        card: this.fb.group({
            name: ['', Validators.required]
        }),
        payment_method: [''],
        PaymentMethodId: ['']
        
    });
    public customPatterns = { '0': { pattern: new RegExp('-|[0-9]') }, '9': { pattern: new RegExp('[0-9]') } };
    public idx: number;
    public policy: any;

    public cardOptions: ElementOptions = {
        style: {
            base: {
                iconColor: '#666EE8',
                color: '#31325F',
                lineHeight: '40px',
                fontWeight: 300,
                fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                fontSize: '18px',
                '::placeholder': {
                    color: '#CFD7E0'
                }
            }
        },
        hidePostalCode: false
    };

    public elementsOptions: ElementsOptions = {
        locale: 'en',
    };

    public loading: boolean;

    constructor(
        public dialogRef: MatDialogRef<ModalPaymentOutComponent>,
        @Optional() @Inject(MAT_DIALOG_DATA) public data: any,
        private fb: FormBuilder,
        private pay: PaymentService,
        public cart: CartService,
        private stripe: StripeService,
        public auth: AuthService
    ) { }

    ngOnInit() {
       this.form.patchValue({
            payment_method: this.data.method.type,
            PaymentMethodId: this.data.method.id
       });
    }

    get bankAccountForm() {
        return this.form.get('account') as FormGroup;
    }

    protected saveMethod(response) {
        if (response.error) {
            this.form.setErrors({ stripe: response.error.message })
            this.loading = false;
        } else {
            this.pay.update({
                PaymentMethodId: this.form.get('PaymentMethodId').value,
                PaymentToken: response.token.id,
                CustomerMail: this.auth.user.email
            }).subscribe((response) => {
               
                if(!this.data.account) {
                    this.pay.editPaymode(this.data.idx, response);
                    this.dialogRef.close();
                } else {
                    this.dialogRef.close(response);
                }
              
            });
        }
    }

    public addCardMethod() {
        let name = this.auth.user.name;
        this.stripe.createToken(this.card.getCard(), { name }).subscribe((response) => this.saveMethod(response));
        
    }

    public addBankMethod() {
        this.stripe.createToken('bank_account', this.bankAccountForm.value).subscribe((response)  => this.saveMethod(response));
    }

    public save() {
        if (this.form.get('payment_method').value == 'card') {
            this.addCardMethod();
        } else if (this.form.get('payment_method').value == 'bank') {
            this.addBankMethod();
        }
    }
}
