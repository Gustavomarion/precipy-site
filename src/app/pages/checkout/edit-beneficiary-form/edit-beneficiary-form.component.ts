import { Component, OnInit, Input, Inject, Optional } from '@angular/core';
import { Policy } from 'src/app/@Ebao/models/Policy/policy';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Customer } from 'src/app/@Ebao/models/Customer/customer';
import { AuthService } from 'src/app/services/auth.service';
import { CustomerService } from 'src/app/@Ebao/services/customer.service';
import { CartService } from 'src/app/services/cart.service';
import { PaymentService } from 'src/app/@Ebao/services/payment.service';
import { debounceTime, distinct } from 'rxjs/operators';

@Component({
    selector: 'app-edit-beneficiary-form',
    templateUrl: './edit-beneficiary-form.component.html',
    styleUrls: ['./edit-beneficiary-form.component.scss', '../checkout.component.scss']
})
export class EditBeneficiaryFormComponent implements OnInit {

    public form: FormGroup = this.fb.group({
        BeneficiaryName: ['', Validators.required],
        BeneficiaryEmail: ['', [Validators.required, Validators.email]],
        BeneficiaryMessage: [''],
        BeneficiaryPersist: [false],
        BeneficiaryId: [''],
        saves: ['']
    });

    public beneficiaries: any[];

    public policy: Policy;
    public idx: number;
    constructor(
        public dialogRef: MatDialogRef<EditBeneficiaryFormComponent>,
        @Optional() @Inject(MAT_DIALOG_DATA) public data: any,
        public pay: PaymentService,
        private fb: FormBuilder,
        public auth: AuthService,
        public cart: CartService
    ) { }

    ngOnInit() {
        this.policy = this.data.policy;
        this.idx = this.data.idx;

        this.form.patchValue(this.policy);
        this.form.get('saves').setValue(this.policy.BeneficiaryName);
        if(!this.pay.beneficiaries.length) {
            this.pay.getBeneficiaries(this.auth.user.email).subscribe(response => {
                console.log(response)
            });
        }

        this.form.get('BeneficiaryName').valueChanges.subscribe(() => {
            this.form.get('saves').setValue('');
        });
        const policyHolder = new Customer({
            CustomerName: this.auth.user.name,
            Email: this.auth.user.email
        });

        policyHolder.policyHolder();
        policyHolder.removeEmpty()
        this.policy.PolicyCustomerList = [policyHolder];

    
    }




    public select({value}) {
        // this.policy.BeneficiaryPersist = this.policy.BeneficiaryPersist? 'Y': 'N';
        value = this.pay.beneficiaries.find(e => e.individual.first_name == value);
        this.form.patchValue({
            BeneficiaryName: value.individual.first_name,
            BeneficiaryId: value.id,
            BeneficiaryEmail: value.email
        });
        // this.dialogRef.close()
    }
    public save() {
        const data = this.form.value;

        let hasID =  this.pay.beneficiaries.some((e) => {
            if(String(e.email).includes(data.BeneficiaryEmail.trim()) ) {
                return true;
            }
        });
        data.BeneficiaryPersist = '';
        if(!hasID) {
            data.BeneficiaryId = '';
            data.BeneficiaryPersist = data.BeneficiaryPersist? 'Y': 'N';
        }

        delete data.saves;
        this.policy = {...this.policy, ...data};

        this.cart.edit(this.idx, this.policy);
        this.dialogRef.close()
    }
}
