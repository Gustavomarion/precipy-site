import { Component, OnInit, Inject, Optional } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { CustomerService } from 'src/app/@Ebao/services/customer.service';
import { CartService } from 'src/app/services/cart.service';
import { Customer } from 'src/app/@Ebao/models/Customer/customer';
import { StripeService, TokenResult } from 'ngx-stripe';
import { PaymentService } from 'src/app/@Ebao/services/payment.service';
import { AuthService } from 'src/app/services/auth.service';
import { Policy } from 'src/app/@Ebao/models/Policy/policy';
import { PolicyService } from 'src/app/@Ebao/services/policy.service';

@Component({
    selector: 'app-modal-payment-out',
    templateUrl: './modal-payment-out.component.html',
    styleUrls: ['./modal-payment-out.component.scss', '../checkout.component.scss']
})
export class ModalPaymentOutComponent implements OnInit {
    public form: FormGroup = this.fb.group({
        ebao: this.fb.group({
            BeneficiaryPayoutLast4: [''],
            BeneficiaryPayoutToken: [''],
            BeneficiaryPayoutPersist: [false],
        }),
        stripe: this.fb.group({
            account_holder_name: ['', Validators.required],
            routing_number: ['', Validators.required],
            account_number: ['', Validators.required],

            // dados fixos
            account_holder_type: ['individual'],
            country: ['US'],
            currency: ['usd']
        })
    });
    public idx: number;
    public policy: Policy;

    public payouts: any[] = [];
    public loading: boolean;
    constructor(
        public dialogRef: MatDialogRef<ModalPaymentOutComponent>,
        @Optional() @Inject(MAT_DIALOG_DATA) public data: any,
        private fb: FormBuilder,
        private customerService: CustomerService,
        private pay: PaymentService,
        public cart: CartService,
        private stripe: StripeService,
        public auth: AuthService,
        private policyService: PolicyService
    ) { }

    get stripeForm() {
        return this.form.get('stripe') as FormGroup;
    }

    get ebaoForm() {
        return this.form.get('ebao') as FormGroup;
    }
    ngOnInit() {
        this.idx = this.data.idx;
        // this.save()
        this.policy = this.data.policy;
        this.stripeForm.patchValue({
            account_holder_name: this.policy.BeneficiaryName,
            
        });
        console.log(this.policy)
        if(this.policy.BeneficiaryId) {
            this.pay.getPayouts(this.policy.BeneficiaryId).subscribe((response: any ) => {
                this.payouts = response.external_accounts.data;
                console.log(response)
            });
        }
        
    }



    public select({value}) {
        // BeneficiaryPayoutId: response.token.id,
        this.ebaoForm.setControl('BeneficiaryPayoutId', this.fb.control(value.bank_account.id));

        this.cart.items[this.idx].BeneficiaryPayoutPersist = this.cart.items[this.idx].BeneficiaryPayoutPersist ? 'Y': 'N';
        this.cart.edit(this.idx, {
            ...this.cart.items[this.idx],
            BeneficiaryPayoutLast4: value.last4,
            BeneficiaryPayoutId: value.id
        });
        this.dialogRef.close();
    }

    public save() {
        // this.cart.items[this.idx].BeneficiaryPayoutToken = 'token';
        // this.cart.items[this.idx].BeneficiaryPayoutLast4 = 'os ultimos numeros';
        // this.cart.items[this.idx].BeneficiaryPayoutId = 'id do pagamento';
        this.ebaoForm.removeControl('BeneficiaryPayoutId');
        this.loading = true;
        this.stripe.createToken('bank_account', this.stripeForm.value).subscribe((response: TokenResult) => {
            if(response.token) {
                this.ebaoForm.patchValue({
                    BeneficiaryPayoutLast4: response.token.bank_account.last4,
                    BeneficiaryPayoutToken: response.token.id,
                });


                let data = this.ebaoForm.value;

                data.BeneficiaryPayoutPersist = data.BeneficiaryPayoutPersist ? 'Y': 'N';

                if(this.data.edit) {
                    this.policyService.updateBeneficiary({
                        PolicyNo: this.policy.PolicyNo,
                        ...data
                    }).subscribe((response) => {
                        // console.log(response)
                        this.dialogRef.close(response.NewPolicy);
                    }).add(() =>    this.loading = false);
                } else {
                    this.cart.edit(this.idx, {
                        ...this.cart.items[this.idx],
                        ...data
                    });
                    this.dialogRef.close();
                    this.loading = false;
                }
                
            }
            if(response.error) {
                this.form.setErrors({stripe: response.error.message})
                this.loading = false;
            }
        });
    }

}
