import { Component, OnInit } from '@angular/core';
import { HelperService } from '../../services/helper.service';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {

  constructor(public helper: HelperService, private route: ActivatedRoute) { }

  public scroll(id, effect = true): void {
    const el = document.getElementById(id);
    if (effect) {
        el.scrollIntoView({
            behavior: 'smooth'
        });
    } else {
        el.scrollIntoView();
    }
}

  ngOnInit() {

    this.route.params.subscribe((params: any) => {
      if (params.section) {
        this.scroll(params.section);
      }
    });
  }

}
