import { HelperService } from './../../services/helper.service';
import { ActivatedRoute } from '@angular/router';
import { services } from './../../.shared/services';
import { Component, OnInit } from '@angular/core';
import { $ } from 'protractor';
import * as Jquery from 'jquery';
import { ProductService } from 'src/app/@Ebao/services/product.service';
import { debounceTime, retry } from 'rxjs/operators';


@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

    public services: any[] = services;
    public service: any = services[0];

    public segments: any = {
        1: [],
        2: []
    };
    constructor(
        private route: ActivatedRoute,
        public helper: HelperService,
        private productService: ProductService
    ) { }



    ngOnInit() {

        this.route.params.subscribe((params: any) => {
            if (params.section) {
                this.scroll(params.section);
            }
        });


        this.productService.info('EVENT_CATEGORY').pipe(
            debounceTime(300),
            retry(2)
        ).subscribe((response: any[]) => {
            // console.log(response);

            response.forEach((e: any) => {
                for(let conditionField of   e.ConditionFields) {
                    this.segments[ conditionField.ID].unshift(e) ;
                }
            });

            console.table(this.segments)
        });
    }

    public pickService(id) {
        this.service = this.services.find((service) => service.id === id);
    }

    public scroll(id, effect = true): void {
        const el = document.getElementById(id);
        if (effect) {
            el.scrollIntoView({
                behavior: 'smooth'
            });
        } else {
            el.scrollIntoView();
        }
    }

}
