import { Component, OnInit } from '@angular/core';
import { PolicyService } from 'src/app/@Ebao/services/policy.service';
import { ActivatedRoute } from '@angular/router';
import { HelperService } from 'src/app/services/helper.service';
import { retry } from 'rxjs/operators';
import { Policy } from 'src/app/@Ebao/models/Policy/policy';

@Component({
    selector: 'app-policy',
    templateUrl: './policy.component.html',
    styleUrls: ['./policy.component.scss'],
    providers: [PolicyService]
})
export class PolicyComponent implements OnInit {
    public policy: Policy | any;
    constructor(
        private policyService: PolicyService,
        private route: ActivatedRoute,
        public helper: HelperService
    ) { }

    ngOnInit() {


        this.route.params.subscribe((params: any) => {
            this.policyService.cleanOptions().find(params.id).pipe(
                retry(2)
            ).subscribe((response) => {
                this.policy = response;
                this.policy.PolicyLobList[0].PolicyRiskList[0].Latitude = parseFloat(String(this.policy.PolicyLobList[0].PolicyRiskList[0].Latitude));
                this.policy.PolicyLobList[0].PolicyRiskList[0].Longitude = parseFloat(String(this.policy.PolicyLobList[0].PolicyRiskList[0].Longitude));
                console.log(this.policy)

            });
        });
    }

}
