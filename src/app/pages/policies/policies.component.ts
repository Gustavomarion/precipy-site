import { Component, OnInit } from '@angular/core';
import { Policy } from 'src/app/@Ebao/models/Policy/policy';
import { ActivatedRoute, Router } from '@angular/router';
import { PolicyService } from 'src/app/@Ebao/services/policy.service';
import { PolicyRequest } from 'src/app/@Ebao/models/Policy/policy-request';
import { retry } from 'rxjs/operators';
import { HelperService } from 'src/app/services/helper.service';
import { Subject } from 'rxjs';

@Component({
    selector: 'app-policies',
    templateUrl: './policies.component.html',
    styleUrls: ['./policies.component.scss']
})
export class PoliciesComponent implements OnInit {
    public policies: Policy[] = [];
    public params: any;
    public options: any = {
        upcoming: {
            filter: false,
            cancel: true,
            editBeneficiary: false,
            editPayout: true,
            count: 0,
            rules: {
                FromRangeConditions:  {
                    ExpiryDate: this.helper.date().format('Y-MM-DD')
                },
                Conditions:  {
                    ProposalStatus: 3,
                    PolicyStatus: 2
                }
            }
        },
        'rained-or-snowed': {
            filter: false,
            cancel: false,
            editBeneficiary: false,
            editPayout: false,
            addPayout: true,
            count: 0,
            claim: true,
            Module: 'ClaimCase',
            rules: {
                
            }
        },
        cancelled: {
            filter: false,
            cancel: false,
            editBeneficiary: false,
            editPayout: false,
            count: 0,
            rules: {
                Conditions:  {
                    ProposalStatus: 3,
                    PolicyStatus: 3
                }
            }
        },
        all: {
            filter: true,
            cancel: false,
            editBeneficiary: false,
            editPayout: false,
            count: 0
        },
        Expired: {
            rules: {
                ToRangeConditions:  {
                    ExpiryDate: this.helper.date().add(-1, 'day').format('Y-MM-DD')
                },
                Conditions:  {
                    ProposalStatus: 3,
                    PolicyStatus: 2
                },
            }
        }
    };

    public option: any = {rules: {}};

    public allowedFilters: any[]= ['ExpiryDate', 'EffectiveDate'];

    public cleanFilters: Subject<boolean> = new Subject();
    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private policyService: PolicyService,
        public helper: HelperService
    ) { }

    ngOnInit() {


        for(let i in this.options) {
            if(  !i.includes('Expired')) {
                this.calcFields(this.options[i], i);
            }
        }
        this.route.params.subscribe((params: any) => {
            if(this.options[params.option]) {
                this.option = this.options[params.option]; 
            } else {
                this.router.navigate(['/not-found']);
            } 
            this.params = params;
           
            this.get();
        });

    }

    public prepareFilters(option) {
        const policyRequest = new PolicyRequest();
        if(option.Module) {
            policyRequest.Module = option.Module;
        }
        for(let i in option.rules) {
            policyRequest[i] = {
                ...policyRequest[i],
                ...option.rules[i]
            };
        }

        // for(let i in this.params) {
        //     if(this.allowedFilters.indexOf(i) > -1 && this.params[i] !== 'null' &&  this.params[i] !== undefined && this.params[i]) {
        //         policyRequest.Conditions[i] = this.params[i];
        //     }
        // }

        return policyRequest;
    }

    public get() {
        let option = this.options[this.params.Status] ? this.options[this.params.Status]  : this.option
        let policyRequest = this.prepareFilters( option);
        policyRequest.SortField = this.params.SortField ? this.params.SortField: 'index_time';
        policyRequest.SortType = this.params.SortType ? this.params.SortType: 'desc';
        
        // console.log(policyRequest)


        // console.log(Object.keys(this.params), 'keys')
        if(option.filter &&  this.policies.length> 0 && Object.keys(this.params).length < 2 ) {
            this.cleanFilters.next(true);
        }

        this.policies = [];
        this.policyService.getAll(policyRequest, option.claim).pipe(
            retry(2)
        ).subscribe((response: any) => {
            this.policies = response.list;
            if(option.claim) {
                this.policies.map((e: any) => e.Claimed = true)
            }
            // console.log(this.policies)
        });
    }

    public calcFields(option, idx) {
        let policyRequest = this.prepareFilters(option);
        this.policyService.getAll(policyRequest, option.claim).pipe(
            retry(2)
        ).subscribe((response: any) => {
            this.options[idx].count = response.Total;
        });
    }

    public addCanceled($event) {
        this.options.cancelled.count++;
        this.options.upcoming.count--;
    }
}
