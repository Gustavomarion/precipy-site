import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Policy } from 'src/app/@Ebao/models/Policy/policy';
import { ProductService } from 'src/app/@Ebao/services/product.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { SatDatepicker, MAT_DATE_FORMATS } from 'saturn-datepicker';
import { HelperService } from 'src/app/services/helper.service';
import { MatDialog } from '@angular/material';
import { EditBeneficiaryFormComponent } from '../../checkout/edit-beneficiary-form/edit-beneficiary-form.component';
import { ModalPaymentOutComponent } from '../../checkout/modal-payment-out/modal-payment-out.component';
import { timeList } from 'src/app/.shared/times';
import { debounceTime, distinct } from 'rxjs/operators';
import { PolicyService } from 'src/app/@Ebao/services/policy.service';
import { Subject } from 'rxjs';

@Component({
    selector: 'app-item',
    templateUrl: './item.component.html',
    styleUrls: ['./item.component.scss'],
    providers: [
        {
            provide: MAT_DATE_FORMATS, useValue: {
                parse: {
                    dateInput: ['DD-MM-Y'],
                },
                display: {
                    dateInput: 'DD-MM-Y',
                    monthYearLabel: 'MMM YYYY',
                    dateA11yLabel: 'DD-MM-Y',
                    monthYearA11yLabel: 'MMMM YYYY',
                },
            }
        },
    ]
})
export class ItemComponent implements OnInit {
    @Input() public policies: Policy[];
    @Input() public conf: any;
    @Input() public cleanFilters: Subject<boolean>;
    @Output() public canceledTrigger: EventEmitter<any> = new EventEmitter();
    public timeList: any[] = timeList;
    public params: any;
    public form: FormGroup = this.fb.group({
        SortType: [''],
        EffectiveDate: [''],
        Status: ['all'],
        SortField: ['IssueDate']
    });

    public cancelingIdx: number;
    public skipTrigger: boolean;
    constructor(
        public productService: ProductService,
        private policyService: PolicyService,
        private fb: FormBuilder,
        private router: Router,
        private route: ActivatedRoute,
        public helper: HelperService,
        public dialog: MatDialog
    ) { }


    ngOnInit() {
        this.form.patchValue(this.route.snapshot.params);



        this.cleanFilters.subscribe((val) => {
            if (val) {
                this.form.patchValue({
                    SortType: '',
                    EffectiveDate: '',
                    Status: 'all',
                    SortField: 'IssueDate'
                });
                this.skipTrigger = true;
            }else {
                this.skipTrigger = false;
            }
        });

        

        this.form.valueChanges.pipe(
            debounceTime(800),
            distinct()
        ).subscribe(() => {
            let data = this.form.value;
            if (this.helper.date(data.EffectiveDate).isValid()) {
                data.EffectiveDate = this.helper.date(data.EffectiveDate).format('Y-MM-DD');
            }
            console.log(  this.skipTrigger )
            if(!this.skipTrigger) {
                this.router.navigate(['/policies', this.route.snapshot.params.option, this.form.value]);
            }else {
                this.skipTrigger = false;
            }
        });


    }


    public editBeneficiary(policy, idx) {
        const dialogRef = this.dialog.open(EditBeneficiaryFormComponent, {
            width: 'auto',
            maxWidth: '781px',
            height: 'auto',
            data: { idx: idx, policy: policy },
            id: 'edit-beneficiary'
        });

    }

    public paymentOut(policy, idx) {
        const dialogRef = this.dialog.open(ModalPaymentOutComponent, {
            width: 'auto',
            minWidth: '500px',
            maxWidth: '781px',
            height: 'auto',
            data: { idx: idx, policy: policy, edit: true },
            id: 'payment-out-modal'
        });

        dialogRef.afterClosed().subscribe((policy: Policy) => {
            if (policy) {
                // this.policies[idx] = policy;
                this.policies[idx].BeneficiaryPayoutLast4 = policy.BeneficiaryPayoutLast4;
                // this.policies[0].PolicyLobList[0].PolicyRiskList[0].PolicyCoverageList[0].RainIntensity
            }
        });

    }

    public cancel(idx) {

        this.cancelingIdx = idx;

        this.policyService.cancel(this.policies[idx].PolicyNo).subscribe((response: any) => {
            console.log(response)
            this.policies.splice(idx, 1);
            this.canceledTrigger.emit(response.NewPolicy);
        }).add(() => {
            this.cancelingIdx = null;
        })
    }
}
