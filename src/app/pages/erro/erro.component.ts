import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-erro',
    templateUrl: './erro.component.html',
    styleUrls: ['./erro.component.scss']
})
export class ErroComponent implements OnInit {
    constructor(public route: ActivatedRoute) { }

    ngOnInit() {
        if (this.route.snapshot.params.no_connection && window.navigator.onLine) {
           
            setTimeout(() =>  window.history.back(), 1000);
        }
    }

}
