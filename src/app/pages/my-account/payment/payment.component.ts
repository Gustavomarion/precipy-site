import { Component, OnInit, ViewChild } from '@angular/core';
import { PaymentService } from 'src/app/@Ebao/services/payment.service';
import { PaymentMethod } from 'ngx-stripe/lib/interfaces/payment-intent';
import { AuthService } from 'src/app/services/auth.service';
import { StripeCardComponent, ElementOptions, ElementsOptions, StripeService } from 'ngx-stripe';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { HelperService } from 'src/app/services/helper.service';
import { MatDialog } from '@angular/material';
import { EditPaymentComponent } from '../../checkout/pay/edit-payment/edit-payment.component';

@Component({
    selector: 'app-payment',
    templateUrl: './payment.component.html',
    styleUrls: ['./payment.component.scss']
})
export class PaymentComponent implements OnInit {
    public methods: PaymentMethod[] = [];
    public editIdx: number;
    @ViewChild(StripeCardComponent, { static: false }) card: StripeCardComponent;
    public showHideNewMethod;
    public customPatterns = { '0': { pattern: new RegExp('-|[0-9]') }, '9': { pattern: new RegExp('[0-9]') } };
    public form: FormGroup = this.fb.group({
        card: this.fb.group({
            name: ['', Validators.required]
        }),
        account: this.fb.group({
            account_holder_name: ['', Validators.required],
            routing_number: ['', Validators.required],
            account_number: ['', Validators.required],
            // dados fixos
            account_holder_type: ['individual'],
            country: ['US'],
            currency: ['usd']
        }),
        payment_method: ['card']
    });

    cardOptions: ElementOptions = {
        style: {
            base: {
                iconColor: '#666EE8',
                color: '#31325F',
                lineHeight: '40px',
                fontWeight: 300,
                fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                fontSize: '18px',
                '::placeholder': {
                    color: '#CFD7E0'
                }
            }
        },
        value: {
        },
        hidePostalCode: false
    };

    elementsOptions: ElementsOptions = {
        locale: 'en',
    };

    public loadings: any = {};
    public deletingIdx: number;
    constructor(
        private stripe: StripeService,
        private fb: FormBuilder,
        public helper: HelperService,
        public pay: PaymentService,
        private auth: AuthService,
        public dialog: MatDialog,
    ) { }

    ngOnInit() {
        this.pay.payModes({
            CustomerMail: this.auth.user.email
        }).subscribe((response: PaymentMethod[]) => {
            this.methods = response;
            console.log(this.methods)
        });
    }

    get cardForm() {
        return this.form.get('card') as FormGroup;
    }

    get bankAccountForm() {
        return this.form.get('account') as FormGroup;
    }
    public edit(idx) {
        this.editIdx = idx;
    }   


    protected saveMethod(response) {
        if (response.error) {
            this.form.setErrors({ stripe: response.error.message })
            this.loadings.addPaymentMethod = false;
        } else {
            this.pay.create({
                PaymentToken: response.token.id,
                CustomerMail: this.auth.user.email
            }).subscribe((response) => {
                this.methods.push(response);
                this.showHideNewMethod = false;
                this.cardForm.reset();
            }).add(() => this.loadings.addPaymentMethod = false);
        }
    }

    public addCardMethod() {
        let name = this.auth.user.name;
        this.stripe.createToken(this.card.getCard(), { name }).subscribe((response) => this.saveMethod(response));
    }


    public removeMethod(id, idx) {
        // console.log(id);
        this.deletingIdx = idx;
        this.pay.delete(id).subscribe((response) => {
            this.methods.splice(idx, 1);
        }).add(() => this.deletingIdx = null);
    }

    public addBankMethod() {
        this.stripe.createToken('bank_account', this.bankAccountForm.value).subscribe((response)  => this.saveMethod(response));
    }

    public methodEdit(method, idx) {
        const dialogRef = this.dialog.open(EditPaymentComponent, {
            width: 'auto',
            maxWidth: '781px',
            height: 'auto',
            data: {idx: idx, method: method, account: true},
            id: 'payment-modal'
        });

        dialogRef.afterClosed().subscribe(val => {
            if(val) {
                this.methods[idx] = val;
            }
        });
    }
    public submit() {
        this.loadings.addPaymentMethod = true;
        if (this.form.get('payment_method').value == 'card') {
            if(this.cardForm.valid) {
                this.addCardMethod();
            } else {
                this.form.setErrors({ stripe: 'Please, complete the informations.' })
                this.loadings.addPaymentMethod = false;
            }
         
        } else if (this.form.get('payment_method').value == 'bank') {
          

            if(this.bankAccountForm.valid) {
                this.addBankMethod();
            } else {
                this.form.setErrors({ stripe: 'Please, complete the informations.' })
                this.loadings.addPaymentMethod = false;
            }
        }
    }
}
