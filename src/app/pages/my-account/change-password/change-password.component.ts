import { Component, OnInit, ChangeDetectorRef, AfterViewChecked } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { FirebaseError } from 'firebase';

@Component({
    selector: 'app-change-password',
    templateUrl: './change-password.component.html',
    styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit, AfterViewChecked {
    public form: FormGroup = this.fb.group({
        password: ['', Validators.required],
        confirm_password: ['', Validators.required],
        current_password: ['', Validators.required]
    });
    public reseted: boolean;
    public loading: boolean;
    constructor(
        private fb: FormBuilder,
        private auth: AuthService,
        private cd: ChangeDetectorRef
    ) { }

    get password() {
        return this.form.get('password');
    }

    get confirm_password() {
        return this.form.get('confirm_password');
    }

    get errorSame() {
        let errorSame = false;
        if(this.confirm_password.value !== this.password.value && this.password.touched && this.confirm_password.touched) {
            this.confirm_password.setErrors({not_match: true})
            errorSame = true;
        } else {
            this.confirm_password.setErrors(null)
        }
        return errorSame ;
    }
    ngOnInit() {
        // this.errorSame
        this.form.valueChanges.subscribe(() => this.cd.detectChanges())
    }

    ngAfterViewChecked() {
        this.cd.detectChanges()
    }

    changePassword() {
        this.loading = true;
        this.auth.afAuth.auth.signInWithEmailAndPassword(this.auth.user.email, this.form.get('current_password').value).then(() => {
            this.auth.updatePassword(this.password.value).subscribe((res) => {
                res.then(val => {
                    this.reseted = true;
                }).catch((reason: FirebaseError) => {
                    console.log(reason);
                    this.password.setErrors({fire: reason.message})
                }).finally(() => this.loading = false)
            });
        }).catch((reason: FirebaseError) => {
            this.form.get('current_password').setErrors({fire: reason.message})
        }).finally(() =>  this.loading = false)
        
    }
}
