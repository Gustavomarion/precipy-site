import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-personal',
  templateUrl: './personal.component.html',
  styleUrls: ['./personal.component.scss']
})
export class PersonalComponent implements OnInit {

  public form: FormGroup = this.fb.group({
    name: [''],
    email: ['', Validators.required]
  });

  public msgUpdate: boolean;
  public loading: boolean;
  constructor(
    public auth: AuthService,
    private fb: FormBuilder,
    private userService: UserService
  ) { }

  ngOnInit() {
    this.form.patchValue(this.auth.user)
  }


  save() {
    this.loading = true;
    this.userService.update(this.auth.user.uid, this.form.value).then(() => {
      this.msgUpdate = true;
      setTimeout(() => this.msgUpdate = false, 5000);
    }).finally(() => {
      this.loading = false;
    })
  }

}
