import { Component, OnInit } from '@angular/core';
import { PaymentService } from 'src/app/@Ebao/services/payment.service';
import { AuthService } from 'src/app/services/auth.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
    selector: 'app-beneficiary',
    templateUrl: './beneficiary.component.html',
    styleUrls: ['./beneficiary.component.scss']
})
export class BeneficiaryComponent implements OnInit {
    public beneficiaries: any[] = [];
    public form: FormGroup = this.fb.group({
        BeneficiaryName: ['', Validators.required],
        BeneficiaryEmail: ['', [Validators.required, Validators.email]],
        BeneficiaryPersist: [true],
        BeneficiaryId: [''],
        CustomerMail: ['']
    });

    public formNew: FormGroup = this.fb.group({
        BeneficiaryName: ['', Validators.required],
        BeneficiaryEmail: ['', [Validators.required, Validators.email]],
        BeneficiaryPersist: [true],
        CustomerMail:[this.auth.user.email],
    });
    public editIdx: number;
    public deleteIdx: number;
    public new: boolean;
    public loading: any = {};
    constructor(
        public pay: PaymentService,
        private auth: AuthService,
        private fb: FormBuilder
    ) { }

    ngOnInit() {
        this.pay.getBeneficiaries(this.auth.user.email, true).subscribe((response) => {
            this.beneficiaries = response;
        });
    }

    public edit(idx) {
        this.editIdx = idx;
        this.form.patchValue({
            BeneficiaryName: this.beneficiaries[idx].individual.first_name,
            BeneficiaryEmail: this.beneficiaries[idx].individual.email,
            BeneficiaryId: this.beneficiaries[idx].id,
            CustomerMail: this.auth.user.email
        });

    }

    public delete(idx) {
        this.beneficiaries[idx].id
        this.loading.deleting = true;
        this.deleteIdx = idx
        this.pay.deleteBeneficary({
            CustomerMail: this.auth.user.email,
            BeneficiaryId: this.beneficiaries[idx].id,
            // CustomerId: ''
        }).subscribe((response) => {
            this.beneficiaries.splice(idx, 1);
        }).add(() => {
            this.loading.deleting = false;
            this.deleteIdx = null;
        } )
    }

    public addNew() {
        this.loading.adding = true;
        this.pay.createBeneficary(this.formNew.value).subscribe((response) => {
            // console.log(response);
            this.beneficiaries.push(response);
            this.new = false;
        }).add(() => this.loading.adding = false);
    }

    public save(idx?) {
        this.loading.editing = true;
        this.pay.editBeneficary(this.form.value).subscribe((response) => {
            this.beneficiaries[this.editIdx] = response;
        }).add(() => {
            this.loading.editing = false;  
            this.editIdx = null;
        });
    }

}
