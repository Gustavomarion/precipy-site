import { Component } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';
import { ProductService } from './@Ebao/services/product.service';
import { interval } from 'rxjs';
import { EbaoAuth } from './@Ebao/EbaoAuth';
import { AuthService } from './services/auth.service';
import { environment } from 'src/environments/environment';


@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    title = 'precipy';
    constructor(
        private router: Router,
        public ebaoAuth: EbaoAuth,
        private auth: AuthService,
        private productService: ProductService
    ) {
        // const navEndEvents = router.events.pipe(
        //     filter(event => event instanceof NavigationEnd),
        // );

        // navEndEvents.subscribe((event: NavigationEnd) => {
        //     window.scrollTo({
        //         top: 0,
        //         behavior: 'smooth'
        //     });
        // });



        if(this.auth.isLoggedIn) {
            environment.ebao_tech.headers["x-ebao-br-insured-id"] = this.auth.user.email;
            // console.log('ds', this.auth.user.email)
        }


        if(!window.navigator.onLine){
            this.router.navigate(['/error', {no_connection: 1}]);
        }
        this.ebaoAuth.init();
        // this.productService.init();
    }
}
