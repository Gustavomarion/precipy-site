import { Component, OnInit, Input, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { HelperService } from 'src/app/services/helper.service';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { BehaviorSubject, Subject } from 'rxjs';
import { Policy } from 'src/app/@Ebao/models/Policy/policy';
import { AuthService } from 'src/app/services/auth.service';
import { MapsService } from 'src/app/services/maps.service';
import { PolicyService } from 'src/app/@Ebao/services/policy.service';
import { CartService } from 'src/app/services/cart.service';
import { Router } from '@angular/router';
import { SatDatepickerInputEvent, SatDatepicker, MAT_DATE_FORMATS } from 'saturn-datepicker';
import { PolicyPayment } from 'src/app/@Ebao/models/Policy/policy-payment';
import * as $ from 'jquery';
import { ProductService } from 'src/app/@Ebao/services/product.service';
import {timeList} from 'src/app/.shared/times';
import { debounceTime, distinct } from 'rxjs/operators';

@Component({
    selector: 'app-create-quote',
    templateUrl: './create-quote.component.html',
    styleUrls: ['./create-quote.component.scss', '../nav.component.scss'],
    providers: [{
        provide: MAT_DATE_FORMATS, useValue: {
            parse: {
                dateInput: 'DD/MM/Y',
            },
            display: {
                dateInput: 'DD/MM/Y',
                monthYearLabel: 'MMM YYYY',
                dateA11yLabel: 'LL',
                monthYearA11yLabel: 'MMMM YYYY',
            },
        }
    }]
})
export class CreateQuoteComponent implements OnInit, OnDestroy {



  public hours: any[] = timeList;


    @ViewChild('searchInput', {static: true}) public searchInput: ElementRef;
    public form: FormGroup = this.fb.group({
        step1: this.fb.group({
            AddressMaps: ['', Validators.required],
            EffectiveDate: ['', Validators.required],
            ExpiryDate: ['', Validators.required],
            EffectiveTime: ['', Validators.required],
            ExpiryTime: ['', Validators.required],
            City: [''],
            State: [''],
            PostCode: [''],
            Address: [''],
            FormattedAddress: ['', Validators.required],
            AddressNumber: [''],
            AddressComplement: [''],
            Suburb: [''],
            Latitude: ['', Validators.required],
            Longitude: ['', Validators.required],
            InsuredCategory: ['1'],
            InsuredSegment: ['1'],
        }),
        Beneficiary: this.fb.group({}),

        coverage: this.fb.group({
            RainIntensity: [1],
            SumInsured: [0, Validators.required],
        })
    });

    public redirect: boolean;
    public another_beneficiary = new BehaviorSubject(false);
    public actionPrice: number = 0;

    public policiesPreview: Policy[] = [];
    public loading: any = {};
    public minDateWhere = this.helper.date().add(10, 'day').format();
    public maxDateWhere;
    public quote: Policy;
    public optionsQuote: any = {};

    @Input() public rainItensities: any[] = [];
    public customRain: any;
    @Input() public edit: boolean;
    @Input() public duplicate: boolean;
    constructor(
        public helper: HelperService,
        private fb: FormBuilder,
        public auth: AuthService,
        public maps: MapsService,
        private policyService: PolicyService,
        public cart: CartService,
        private router: Router,
        private productService: ProductService
    ) { }

    get sumInsured() {
        return this.coverageForm.get('SumInsured');
    }
    get beneficiary() {
        return this.form.get('Beneficiary') as FormGroup;
    }

    get coverageForm() {
        return this.form.get('coverage') as FormGroup;
    }

    get step1Form() {
        return this.form.get('step1') as FormGroup;
    }



    ngOnInit() {
        //Jquery('body').css('display', 'none');

        this.maps.getPlace().subscribe(place => {
            this.step1Form.patchValue(place);
        });

        this.another_beneficiary.subscribe((value) => {
            if (value) { // take own info
                this.beneficiary.setControl('BeneficiaryName', this.fb.control('', Validators.required));
                this.beneficiary.setControl('BeneficiaryEmail', this.fb.control('', Validators.required));
                this.beneficiary.setControl('BeneficiaryMessage', this.fb.control(''));
            } else {
                this.beneficiary.removeControl('BeneficiaryName');
                this.beneficiary.removeControl('BeneficiaryEmail');
                this.beneficiary.removeControl('BeneficiaryMessage');
            }
        });

        this.step1Form.patchValue({
            InsuredCategory: this.helper.quoteCategory.id,
            InsuredSegment: this.helper.quoteCategory.segment
        });

        this.form.valueChanges.pipe(
            debounceTime(600),
            distinct()
        ).subscribe(() => {
            if(this.form.valid && this.sumInsured.value > 50) {
                this.calcQuote();
            }
        });

        // console.log(this.rainItensities)

        if(!this.customRain) {
            const idxCustom = this.rainItensities.findIndex((e) => e.Description == 'Custom Rain');
            this.customRain = this.rainItensities[idxCustom];
        }

        if (this.duplicate) {
            this.patchValues(this.cart.editItem)
        }
        if (this.edit) {
            this.patchValues(this.cart.editItem)
        }

        this.helper.closeEvent.subscribe(event => {
            this.resetAll();
        });
    }


    public patchValues(editItem) {
        if (editItem.BeneficiaryName) {
            this.another_beneficiary.next(true);
        }
        const data = {
            ...editItem.PolicyLobList[0],
            ...editItem,
            ...editItem.PolicyLobList[0].PolicyRiskList[0],
            ...{
                AddressMaps: editItem.PolicyLobList[0].PolicyRiskList[0].FormattedAddress,
                WhereDate: {
                    begin: editItem.EffectiveDate,
                    end: editItem.ExpiryDate
                }
            }
        };
        this.step1Form.patchValue(data);
        this.coverageForm.patchValue(editItem.PolicyLobList[0].PolicyRiskList[0].PolicyCoverageList[0]);
        this.beneficiary.patchValue(data);
        this.quote = editItem;
    }

    formatLabel(value: number) {
        if (value >= 1000) {
            return Math.round(value / 1000) + 'k';
        }
        return value;
    }
    public plusCoverage() {
        this.sumInsured.setValue(this.sumInsured.value <= 5000 ? Number(this.sumInsured.value) + 100 : 5000);
    }
    public lessCoverage() {
        this.sumInsured.setValue(this.sumInsured.value >= 200 ? Number(this.sumInsured.value) - 100 : 100);
    }

    public selectDate(event: SatDatepickerInputEvent<any>) {
        this.maxDateWhere = this.helper.date(event.value).add(24, 'hour').format();
    }

    public currentIntensity() {
        return this.rainItensities.find(e => e.Code == this.coverageForm.value.RainIntensity)
    }

    public chooseQuote(el) {
        $('.contentPrice.active').removeClass('active');
        $(el).addClass('active');
    }

    public policy() {
        let data = this.form.value;
        data.step1.ProposalDate = data.step1.EffectiveDate;

       
        const policy = new Policy({
            ...data.step1,
            ...data.Beneficiary
        }).risk(data.step1)
            .coverages(data.coverage)
            .payment({
                IsInstallment: 'N',
                PayModeCode: '30',
                InstallmentType: '1',
                ExpiryDate: data.ExpiryDate,
                InstallmentPeriodCount: '1',
            } as PolicyPayment);

        if (this.auth.isLoggedIn) {
            policy.makePolicyHolder({
                Email: this.auth.user.email,
                CustomerName: this.auth.user.name
            } as any);
        }
        return policy.prepareRequest();
    }


    public getOptions() {
        // console.log(this.rainItensities)
        for (let i of this.rainItensities) {
            if(i.Description != 'Custom Rain') {
                this.coverageForm.patchValue({ RainIntensity: i.Code });
                this.calcQuote(i.Code);
            }

        }

        this.coverageForm.patchValue(this.quote.PolicyLobList[0].PolicyRiskList[0].PolicyCoverageList[0]);
    }

    calcQuote(options?) {
        console.log(this.policy())
        if (this.form.valid) {
            if (options) {
                this.loading[options] = true;
            } else {
                this.loading.creating = true;
            }
            return this.policyService.quickQuotation(this.policy()).subscribe((response: any) => {
                if (options) {
                    this.optionsQuote[options] = response;
                } else {
                    this.quote = response;
                }

                console.log(this.quote)
            }).add(() => {
                if (options) {
                    this.loading[options] = false;
                } else {
                    this.loading.creating = false
                }
            });
        } else {
            this.form.setErrors({ missingData: true });
        }

        // return;
    }

    public save(action = true) {
        if (this.quote) {
            if (action && !this.edit) {
                this.cart.add(this.quote);
                this.actionPrice = 0;
                if (this.redirect) {
                    this.helper.navOpen = false;
                    this.resetAll();
                    this.router.navigate(['/checkout']);
                } else {
                   this.resetAll();
                }
            }

            if (this.edit) {
                this.cart.edit(this.cart.editIdx, this.quote);
                this.helper.navOpen = false;
                this.cart.editIdx = null;
                this.resetAll();
            }
        } else {
            console.warn('calc quote');
        }
    }

    public resetAll() {
        this.step1Form.reset();
        this.sumInsured.setValue(0);
        this.quote = null;
        this.optionsQuote = {};
        this.form.markAsUntouched();
        this.another_beneficiary.next(false);
       
    }
    ngOnDestroy() {
        // this.cart.editIdx = null;
        this.actionPrice = 0;
    }

}
