import { Component, OnInit, ChangeDetectorRef, AfterViewInit, AfterViewChecked, EventEmitter } from '@angular/core';
import { HelperService } from '../../services/helper.service';
import { AuthService } from 'src/app/services/auth.service';
import { MapsService } from 'src/app/services/maps.service';
import { PolicyService } from 'src/app/@Ebao/services/policy.service';
import { CartService } from 'src/app/services/cart.service';
import { ProductService } from 'src/app/@Ebao/services/product.service';
import { LocalStorageService } from 'angular-web-storage';
import * as $ from 'jquery';

@Component({
    selector: 'app-nav',
    templateUrl: './nav.component.html',
    styleUrls: ['./nav.component.scss'],
    providers: [MapsService, PolicyService]
})
export class NavComponent implements OnInit, AfterViewChecked {
    public rainItensities: any[] ;
    public dropdown: boolean;

    public closeMenu: EventEmitter<any> = new EventEmitter();
    constructor(
        public helper: HelperService,
        public auth: AuthService,
        public cart: CartService,
        private productService: ProductService,
        private cd: ChangeDetectorRef,
        private local: LocalStorageService
    ) {

    }
    ngOnInit() {
        let itens = this.local.get('rainIntensities');

        if(itens) {
            this.rainItensities =itens;
            this.productService.infoData = itens;
        }
        this.productService.info('RainIntensity').subscribe((response: any) => {
            this.rainItensities = response;
            this.productService.infoData = response;

            this.local.set('rainIntensities', response);
        });
    }

    close() {
        this.closeMenu.emit('2');
    }
    ngAfterViewChecked() {
        this.cd.detectChanges();
    }


    openDropdown(e: any) {
        if(String(e.relatedTarget.id).includes('menu')) {
            this.dropdown = true
        }
    }

    closeDropdown(e: any) {
        if(!String(e.relatedTarget.id).includes('menu-left') && !String(e.relatedTarget.id).includes('hover')) {
            this.dropdown = false;
        }
    }
}
