export const services: any[] = [
  {
    id: 1,
    name: 'Vacations',
    description: `Vacations are precious.
    Get insurance to cover your plane tickets,
    your hotel, or whatever else you need if it
    rains. We’re flexible!`,
    img: './assets/icons/travel-test.png',
    segment: 1
  },
  {
    id: 2,
    name: 'Sports',
    description: `Football games can shake off
    more rain than a tee time. Choose what
    level of rain to insure and we’ll help you get back in the game.`,
    img: './assets/icons/sports.png',
    segment: 1
  },
  {
    id: 3,
    name: 'Concerts',
    description: `Rain or snow can turn an outdoor concert into a hard pass,
    even if the show goes on. Get enough coverage
    to hit the next date on the tour.`,
    img: './assets/icons/concerts.png',
    segment: 1
  },
  {
    id: 4,
    name: 'Wedding',
    description: `We can’t stop rain or snow, but we can cover the tent rental,
    a longer honeymoon, whatever happily-ever-after needs to get started.`,
    img: './assets/icons/wedding.png',
    segment: 1
  },
  {
    id: 5,
    name: 'Gifting',
    description: `Precipy’s policies are simple enough to give as gifts. Cheer up
    your grandson if his birthday is rainy,
    or your PTA if the picnic gets canceled.`,
    img: './assets/icons/gifiting.png',
    segment: 1
  },
  {
    id: 6,
    name: 'Revenue Loss',
    description: `Rain keeping people away from your street fair?
    Snow keeping your customers inside?
    Insure your events with Precipy.`,
    img: './assets/icons/revenue-loss.png',
    segment: 2
  },
  {
    id: 7,
    name: 'Business Travel',
    description: `If the airport is closed by rain or snow,
    make sure you have enough insurance to go the next conference.
    Maybe the one in Aruba?`,
    img: './assets/icons/business-travel.png',
    segment: 2
  },
  {
    id: 8,
    name: 'Anything else',
    description: ``,
    img: './assets/icons/business-travel.png',
    segment: 1
  },
  {
    id: 9,
    name: 'Anything else',
    description: ``,
    img: './assets/icons/business-travel.png',
    segment: 2
  }
];
