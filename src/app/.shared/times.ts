export const timeList: any[] = [
  {
    publicTime: '12:00 am',
},
    {
        publicTime: '01:00 am',
    },
    {
        publicTime: '02:00 am',
    },
    {
        publicTime: '03:00 am',
    },
    {
        publicTime: '04:00 am',
    },
    {
        publicTime: '05:00 am',
    },
    {
        publicTime: '06:00 am',
    },
    {
        publicTime: '07:00 am',
    },
    {
        publicTime: '08:00 am',
    },
    {
        publicTime: '09:00 am',
    },
    {
        publicTime: '10:00 am',
    },
    {
        publicTime: '11:00 am',
    },
    {
        publicTime: '12:00 pm',
    },
    {
        publicTime: '1:00 pm',
    },
    {
        publicTime: '2:00 pm',
    },
    {
        publicTime: '3:00 pm',
    },
    {
        publicTime: '4:00 pm',
    },
    {
        publicTime: '5:00 pm',
    },
    {
        publicTime: '6:00 pm',
    },
    {
        publicTime: '7:00 pm',
    },
    {
        publicTime: '8:00 pm',
    },
    {
        publicTime: '9:00 pm',
    },
    {
        publicTime: '10:00 pm',
    },
    {
        publicTime: '11:00 pm',
    }
];
