import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FirebaseError } from 'firebase';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    public params: any;
    public form: FormGroup = this.fb.group({
        email: ['', [Validators.required, Validators.email]],
        password: ['', Validators.required]
    });
    public error: string;
    constructor(
        private route: ActivatedRoute,
        private router: Router,
        public auth: AuthService,
        private fb: FormBuilder
    ) {
    }

    ngOnInit() {
        this.route.params.subscribe((params: any) => this.params = params);
    }

    public login() {
        this.error= '';
        this.auth.helper.startLoading();
        this.auth.signIn(this.form.value.email, this.form.value.password).then(() => {
            if (this.params.checkoutLogin) {
                this.router.navigate(['/checkout']);
            } else {
                this.router.navigate(['/policies']);
            }
        }).catch((error: FirebaseError) => {
            console.log(error)
         
            this.error = 'auth/user-not-found';
            // this.error = error.message;
        }).finally(() => this.auth.helper.stopLoading());
    }
}
