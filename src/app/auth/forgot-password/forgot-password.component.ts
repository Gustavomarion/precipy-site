import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { FirebaseError } from 'firebase';
import { HelperService } from 'src/app/services/helper.service';

@Component({
    selector: 'app-forgot-password',
    templateUrl: './forgot-password.component.html',
    styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {
    public params: any;
    public form: FormGroup = this.fb.group({
        email: ['', [Validators.required, Validators.email]],
    });
    public error: string;
    constructor(
        private route: ActivatedRoute,
        private router: Router,
        public auth: AuthService,
        private fb: FormBuilder,
        public helper: HelperService
    ) {
    }

    ngOnInit() {
        this.route.params.subscribe((params: any) => this.params = params);
    }

    send() {
        this.auth.helper.startLoading();
        this.auth.forgotPassword(this.form.value.email).then(() => {
            this.router.navigate(['/auth/forgot-password', {sended: 1, email: this.form.value.email}]);
        }).catch((reason: FirebaseError) => {
            this.form.setErrors({fire: reason.message})
            
        }).finally(() => this.auth.helper.stopLoading())
    }

}
