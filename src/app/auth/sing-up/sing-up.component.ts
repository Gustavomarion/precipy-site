import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { debounceTime, distinct } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';
import { FirebaseError } from 'firebase';

@Component({
    selector: 'app-sing-up',
    templateUrl: './sing-up.component.html',
    styleUrls: ['./sing-up.component.scss']
})
export class SingUpComponent implements OnInit {

    public form: FormGroup = this.fb.group({
        name: ['', Validators.required],
        email: ['', [Validators.required, Validators.email]],
        password: ['', [Validators.required, Validators.minLength(6)]],
        confirm_password: ['', [Validators.required, Validators.minLength(6)]]
    });

    public submited: boolean;
    public params: any;
    public loading: boolean;
    constructor(
        public auth: AuthService,
        private fb: FormBuilder,
        private router: Router,
        private route: ActivatedRoute
    ) { }

    ngOnInit() {
        this.route.params.subscribe((params: any) =>this.params = params);
    }

    get hasPassword() {
        return this.form.get('password').value &&  this.form.get('confirm_password').value;
    }
    
    public isSamePassword() {
        this.form.get('password').setErrors(null)
        this.form.get('confirm_password').setErrors(null);
        if(this.form.value.password != this.form.value.confirm_password) {
            this.form.get('password').setErrors({noEqual: true});
            this.form.get('confirm_password').setErrors({noEqual: true});
        }
    }

    public register() {
        this.submited = true;
        this.isSamePassword();
        if(this.form.valid) {
            this.loading = true;
            this.auth.signUp(this.form.value).then(() => {

                if (this.params.checkoutLogin) {
                    this.router.navigate(['/checkout']);
                } else {
                    this.router.navigate(['/policies']);
                }
            }).catch((reason: FirebaseError) => {
                if(reason.code.includes('auth/email-already-in-use')){
                    this.form.get('email').setErrors({already_has: true});
                }
            }).finally(() => this.loading = false)
        }
    }

}
