import { MaterialModule } from './../material/material.module';
import { SingUpComponent } from './../auth/sing-up/sing-up.component';
import { LoginComponent } from './../auth/login/login.component';
import { LoggedComponent } from './logged/logged.component';
import { Routes, RouterModule } from '@angular/router';
import { NgModule, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthComponent } from './auth.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';




const routes: Routes = [
  {
    path: '', component: AuthComponent,
    children: [
      { path: 'login', component: LoginComponent },
      { path: 'sign-up', component: SingUpComponent },
      { path: 'forgot-password', component: ForgotPasswordComponent },

      { path: 'status/:active', component: LoggedComponent },
    ]
  }
];


@NgModule({
  declarations: [AuthComponent, LoginComponent, LoggedComponent, SingUpComponent, ForgotPasswordComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MaterialModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class AuthModule { }
