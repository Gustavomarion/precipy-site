import { Injectable, NgZone } from '@angular/core';
import {  Subject } from 'rxjs';
import { MapsAPILoader } from '@agm/core';
import { map, debounceTime, distinct } from 'rxjs/operators';
declare const google: any;

@Injectable({
    providedIn: 'root'
})
export class MapsService  {
    public place: Subject<any> = new Subject();

    constructor(
        private mapsAPILoader: MapsAPILoader,
        private ngZone: NgZone
    ) {
       
    }


    public getPlace() {
        return this.place.pipe(
            debounceTime(100),
            distinct(),
            map((place) => {
                let placeEbao: any = {
                    FormattedAddress: place.formatted_address,
                    Latitude: place.geometry.location.lat(),
                    Longitude: place.geometry.location.lng()
                };
                for(let i of  place.address_components) {
                    if(i.types.indexOf('street_number') > -1) {
                        placeEbao.AddressNumber = i.long_name
                    }
                
                    if(i.types.indexOf('sublocality') > -1 || i.types.indexOf('neighborhood') > -1) {
                        placeEbao.Suburb = i.long_name;
                    }
    
                    if(i.types.indexOf('locality') > -1) {
                        placeEbao.City = i.long_name;
                    }
        
                    if(i.types.indexOf('administrative_area_level_1') > -1) { 
                        placeEbao.State = i.short_name;
                    }
                    if(i.types.indexOf('postal_code_suffix') > -1) {
                        placeEbao.PostCode = i.long_name;
                    }
                    if(i.types.indexOf('route') > -1) {
                        placeEbao.Address = i.long_name;
                    }
                    
                }
                return placeEbao;
            })
        )
    }


  
    public searchCity(input: HTMLInputElement) {
        this.mapsAPILoader.load().then(() => {
            let autocomplete: any = new google.maps.places.Autocomplete(input);
            autocomplete.setTypes(['address']);
            autocomplete.addListener("place_changed", () => {
                this.ngZone.run(() => {
                    let place: any = autocomplete.getPlace();
                    // if() {
                    if(place) {
                        this.place.next(place);
                    }
                })
            });
        });
    }
 

}
