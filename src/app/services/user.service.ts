import { Injectable } from '@angular/core';
import { AngularFirestore, QueryFn } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { CanActivate } from '@angular/router';
import { AuthService } from './auth.service';
@Injectable({
    providedIn: 'root'
})
export class UserService implements CanActivate {
    public collection = 'users';
    items: Observable<any[]>;
    constructor(private db: AngularFirestore) {

    }

    public get() {
        return this.db.collection(this.collection);
    }
    public searchByCpf(cpf) {
        return this.db.collection(this.collection, ref => ref.where('cpf', '==', cpf)).snapshotChanges();
    }

    public find(id) {
        return this.db.collection(this.collection).doc(id).valueChanges();
    }

    public update(uid: string, data: any) {
        return this.db.collection(this.collection).doc(uid).set(data, {merge: true});
    }
    public create(data: any) {
        return this.db.collection(this.collection).doc(data.uid).set(data);
    }

    canActivate() {
        return true;
    }

}
