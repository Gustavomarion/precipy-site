import { Injectable, NgZone, EventEmitter } from '@angular/core';
import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';
import { Observable, BehaviorSubject, Subject } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import * as moment from 'moment';
import { services } from './../.shared/services';
@Injectable({
    providedIn: 'root'
})
export class HelperService {

    public navType = 'nav-trasparent';
    public isMobile: boolean;
    public loading = new BehaviorSubject(false);
    public navOpen: boolean;
    public content = 'nav';

    public userToLogin: any;
    public place: Subject<any> = new Subject();
    public quoteCategory: any = services[0];


    public closeEvent: EventEmitter<any> = new EventEmitter();
    constructor(
        private breakpointObserver: BreakpointObserver,
        public toastr: ToastrService
    ) {
        this.breakpointObserver.observe(['(min-width: 767px)'])
            .subscribe((state: BreakpointState) => {
                if (state.matches) {
                    this.isMobile = false;
                } else {
                    this.isMobile = true;
                }
            });
    }

    get isLoading() {
        return this.loading.getValue();
    }
    public scroll(id, effect = true): void {
        const el = document.getElementById(id);
        if (effect) {
            el.scrollIntoView({
                behavior: 'smooth'
            });
        } else {
            el.scrollIntoView();
        }
    }


    public startLoading() {
        setTimeout(() => this.loading.next(true), 100);
    }

    public stopLoading() {
        setTimeout(() => this.loading.next(false), 100);
    }


    public toggleNav(content: any = 'nav', close: boolean = true) {
        if (close) {
            setTimeout(() => this.navOpen = !this.navOpen, 50);
        }
        
        if(this.navOpen) {
           setTimeout(() => content = '', 50);
        }

        this.content = content;

       
    }

    public openQuote(category?, content = 'nav') {
        this.content = content;
        // this.navOpen = true;
        setTimeout(() => this.navOpen = true, 50);
  
        if( category) {
            this.quoteCategory = category;
        }
    }


    public closeNav(e) {
        // console.log(e.target);
        if (e.target.id === 'menu') {
            this.closeEvent.emit(true);
            this.toggleNav();
        }
    }


    public date(date: any = new Date()) {
        return moment(date)
            .parseZone()
            .locale(window.navigator.language);
    }

}
