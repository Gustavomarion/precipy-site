import { Injectable, NgZone } from '@angular/core';
import { auth, User } from 'firebase/app';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { HelperService } from './helper.service';
import { SessionStorageService, LocalStorageService } from 'angular-web-storage';
import { UserService } from './user.service';
import { map, debounceTime } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})

export class AuthService {
    public token: string;
    public user: any;
    public ebaoToken: string;
    public avatar: string;
    constructor(
        public afs: AngularFirestore,   // Inject Firestore service
        public afAuth: AngularFireAuth, // Inject Firebase auth service
        public router: Router,
        public ngZone: NgZone, // NgZone service to remove outside scope warning,
        public helper: HelperService,
        public session: SessionStorageService,
        private userService: UserService,
        private local: LocalStorageService
    ) {
        /* Saving user data in localstorage when
        logged in and setting up null when logged out */
        this.user = this.session.get('user');
        this.user = this.user? this.user: {};
        this.afAuth.authState.subscribe((user: User) => {
            this.user.email = user.email;
            if (user) {
                this.userService.find(user.uid).subscribe((userData: any) => {
                    this.user.name = userData.name;

                    this.user = { ...userData, uid: user.uid, email: user.email, emailVerified: user.emailVerified};
                    this.session.set('user', this.user);
                });
            }
        });
    }

    // Sign in with email/password
    signIn(email, password) {
        return this.afAuth.auth.signInWithEmailAndPassword(email, password)
            .then((result) => {
                return result;
            });
    }

    // Sign up with email/password
    signUp(form: any) {
        return this.afAuth.auth.createUserWithEmailAndPassword(form.email, form.password)
            .then((result) => {
                /* Call the SendVerificaitonMail() function when new user sign
                up and returns promise */
                this.sendVerificationMail();
                return  this.setUserData({
                    ...{uid: result.user.uid},
                    ...form
                });
            });
    }

    // Send email verfificaiton when new user sign up
    sendVerificationMail() {
        return this.afAuth.auth.currentUser.sendEmailVerification();
    }

    // Reset Forggot password
    forgotPassword(passwordResetEmail) {
        return this.afAuth.auth.sendPasswordResetEmail(passwordResetEmail);
    }

    updatePassword(password: string) {
        return this.afAuth.authState.pipe(
            map(user => {
                return user.updatePassword(password)
            })
        );

    }

    // Returns true when user is looged in and email is verified
    get isLoggedIn(): boolean {
        const user = this.session.get('user');
        return (user !== null) ? true : false;
    }

    // Sign in with Google
    GoogleAuth() {
        return this.authLogin(new auth.GoogleAuthProvider());
    }

    // Auth logic to run auth providers
    authLogin(provider) {
        return this.afAuth.auth.signInWithPopup(provider)
            .then((result) => {
                this.ngZone.run(() => {
                    this.router.navigate(['/admin']);
                });
                // this.setUserData(result.user);
            }).catch((error) => {
                window.alert(error);
            });
    }

    /* Setting up user data when sign in with username/password,
    sign up with username/password and sign in with social auth
    provider in Firestore database using AngularFirestore + AngularFirestoreDocument service */
    setUserData(user) {
        user.emailVerified = false;
        delete user.password;
        delete user.confirm_password;
        user.shopping_cart = this.local.get('itens')? this.local.get('itens'): []
        return this.userService.create(user);
    }

    // Sign out
    signOut() {
        return this.afAuth.auth.signOut().then(() => {
            this.session.remove('user');
            this.session.remove('ebao-token');
            this.router.navigate(['/auth/login']);
        });
    }

}
