import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Policy } from '../@Ebao/models/Policy/policy';
import { LocalStorageService } from 'angular-web-storage';
import { PolicyService } from '../@Ebao/services/policy.service';
import { PolicyCoverage } from '../@Ebao/models/Policy/policy-coverage';
import { map } from 'rxjs/operators';
import { AngularFirestore } from '@angular/fire/firestore';
import { AuthService } from './auth.service';
import { UserService } from './user.service';
@Injectable({
    providedIn: 'root'
})
export class CartService {
    public items: Policy[]| any[]   = [];
    public totalPrice: number;
    public premiumPrice: number;
    public taxPrice: number;

    public editIdx: number;
    constructor(
        private storage: LocalStorageService,
        private user: UserService,
        private auth: AuthService
    ) {


        if (this.auth.isLoggedIn) {
            this.items = this.auth.user.shopping_cart ? this.auth.user.shopping_cart : [];
        }

        if (!this.items.length || !this.auth.isLoggedIn) {
            this.items = this.storage.get('itens');
            this.items = this.items ? this.items : [];
        }
        this.cacheItems();

    }

    get count() {
        return this.items.length;
    }

    get editItem() {
        return this.items[this.editIdx];
    }

    get isAblePayment(): any {
        // console.log(this.items)
        return this.items.every(i => {
            let isAble = false;
            if (i.BeneficiaryPayoutToken || i.BeneficiaryPayoutId) {
                isAble = true;
            }

            if(i.BeneficiaryName) {
                isAble = true;
            }
            return isAble;
        }) && this.items.length > 0;
    }


    protected calc() {
        this.totalPrice = 0;
        this.taxPrice = 0;
        this.premiumPrice = 0;
        this.items.forEach((item) => {
            this.premiumPrice += Number(item.DuePremium);
            this.taxPrice += Number(item.TaxInterest);
        });

        this.totalPrice = this.taxPrice + this.premiumPrice;
    }
    public add(item: Policy | any, opt: any = {}) {
        if (opt.duplicate) {
            item.duplicate = true;
            item.duplicateTime = new Date();
        }
        this.items.push(item);
        this.cacheItems();
    }

    public edit(idx, data) {
        this.items[idx] = data;
        this.cacheItems();
    }

    public remove(idx: number) {
        this.items.splice(idx, 1);
        this.cacheItems();
    }
    public removeAll() {
        this.items = [];
        this.cacheItems();
    }




    public cacheItems() {
        this.calc();
        this.storage.set('itens', this.items);
        if (this.auth.isLoggedIn) {
            this.user.update(this.auth.user.uid, {
                shopping_cart: this.storage.get('itens')
            }).then();
        }

    }


}
