import { MaterialModule } from './material/material.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavComponent } from './layout/nav/nav.component';
import { FooterComponent } from './layout/footer/footer.component';
import { HomeComponent } from './pages/home/home.component';
import { AboutComponent } from './pages/about/about.component';
import { FaqComponent } from './pages/faq/faq.component';
import { TermsComponent } from './pages/terms/terms.component';
import { ParametricComponent } from './pages/terms/parametric/parametric.component';
import { ConditionComponent } from './pages/terms/condition/condition.component';
import { DisclamerComponent } from './pages/terms/disclamer/disclamer.component';
import { LegalComponent } from './pages/terms/legal/legal.component';
import { ErroComponent } from './pages/erro/erro.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HelperService } from './services/helper.service';
import { PoliciesComponent } from './pages/policies/policies.component';

import { PolicyComponent } from './pages/policy/policy.component';
import { CheckoutComponent } from './pages/checkout/checkout.component';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { NgxStripeModule } from 'ngx-stripe';
import { ToastrModule } from 'ngx-toastr';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE, SatDatepickerModule, } from 'saturn-datepicker';
import localeEn from '@angular/common/locales/en';
import { registerLocaleData } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularWebStorageModule } from 'angular-web-storage';
import { AgmCoreModule } from '@agm/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CartService } from './services/cart.service';
registerLocaleData(localeEn, 'en-US');
import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';
import { EditBeneficiaryFormComponent } from './pages/checkout/edit-beneficiary-form/edit-beneficiary-form.component';
import { MyAccountComponent } from './pages/my-account/my-account.component';
import { PersonalComponent } from './pages/my-account/personal/personal.component';
import { BeneficiaryComponent } from './pages/my-account/beneficiary/beneficiary.component';
import { PaymentComponent } from './pages/my-account/payment/payment.component';
import { CreateQuoteComponent } from './layout/nav/create-quote/create-quote.component';
import { UserService } from './services/user.service';
import { AuthService } from './services/auth.service';
import { ModalPaymentOutComponent } from './pages/checkout/modal-payment-out/modal-payment-out.component';
import { PayComponent } from './pages/checkout/pay/pay.component';
import { environment } from 'src/environments/environment';

import { NgxMaskModule, IConfig } from 'ngx-mask';
import { EditPaymentComponent } from './pages/checkout/pay/edit-payment/edit-payment.component';
import { ItemComponent } from './pages/policies/item/item.component';
import { PaymentService } from './@Ebao/services/payment.service';
import { EmptyComponent } from './pages/checkout/empty/empty.component';
import { ChangePasswordComponent } from './pages/my-account/change-password/change-password.component';
export const options: Partial<IConfig> | (() => Partial<IConfig>) = {};

@NgModule({
  entryComponents: [
    EditBeneficiaryFormComponent,
    ModalPaymentOutComponent,
    EditPaymentComponent
  ],
  declarations: [
    AppComponent,
    EditBeneficiaryFormComponent,
    NavComponent,
    FooterComponent,
    HomeComponent,
    AboutComponent,
    FaqComponent,
    TermsComponent,
    ParametricComponent,
    ConditionComponent,
    DisclamerComponent,
    LegalComponent,
    ErroComponent,
    PoliciesComponent,

    PolicyComponent,
    CheckoutComponent,
    MyAccountComponent,
    PersonalComponent,
    BeneficiaryComponent,
    PaymentComponent,
    CreateQuoteComponent,
    ModalPaymentOutComponent,
    PayComponent,
    EditPaymentComponent,
    ItemComponent,
    EmptyComponent,
    ChangePasswordComponent
  ],
  imports: [
    SatDatepickerModule,
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    AngularWebStorageModule,
    NgxStripeModule.forRoot('pk_test_5D6F13T1RGqBXciP9f9DlDWH00BWRYQsYE'),
    ToastrModule.forRoot(),
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule, // imports firebase/firestore, only needed for database features
    AngularFireAuthModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAW4bc8k1GmxfbsYblorAq8JSjz--G7pvU',
      libraries: ['geometry', 'places']
    }),
    FormsModule,
    ReactiveFormsModule,
    NgxMaterialTimepickerModule,
    NgxMaskModule.forRoot(options)
  ],
  providers: [
    HelperService,
    CartService,
    UserService,
    AuthService,
    PaymentService,
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
