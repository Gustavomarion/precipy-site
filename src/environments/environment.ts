// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false,
    ebao_tech: {
        url: 'https://gw.sit.ebaocloud.com/',
        main_path: '/1.0/precipy-bff-app/',
        cas: 'https://cors-anywhere.herokuapp.com/https://sit.ebaocloud.com/cas/',
        tenantcode: 'precipy',
        credentials: {
            username: 'precipy.dev',
            password: 'ZQ8BajvVc3c',
        },
        headers: {
            'x-ebao-tenant-id': 'precipy',
            'x-ebao-br-user-role': 'insured',
            'x-ebao-br-insured-id': ''
        },
        product_schema: {
            productCode: 'PRECP',
            versionDate: '2020-02-11T01:04:13',
            version: '1.0',
            ProductElementCode: 'R10070'
        },
        customer: {
            default_IdNo: ''  // default is email
        },
        coverage: {
            ProductElementCode: 'C101420'
        }
    },

    firebase: {
        apiKey: 'AIzaSyAAlkE4fs6FcqhqLO_6AV1_MRASfM8N6mE',
        authDomain: '',
        databaseURL: 'https://precipy-afdf4.firebaseio.com/',
        projectId: 'precipy-afdf4',
        storageBucket: 'gs://precipy-afdf4.appspot.com/',
        messagingSenderId: '183871279640'
    }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
